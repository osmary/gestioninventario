<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class actualizar_orden_empleado_mod extends CI_Model {

    var $ambiente;
    var $bd_sistema;

    public function __construct() {
        parent::__construct();

        try {
            $this->bd_sistema = new clase_db();
            $this->ambiente = $this->bd_sistema->iniciarConexion('application/models/include/', 'sistema');
            if (!$this->ambiente[0]) {
                throw new Exception($this->ambiente[1]);
            }

            $this->bd_rrhh = new clase_db();
            $this->ambiente_rrhh = $this->bd_rrhh->iniciarConexion('application/models/include/', 'rrhh');
        } catch (Exception $e) {
            error_log('Archivo' . __FILE__ . ' Funcion:' . __FUNCTION__ . ', ' . $e->getMessage(), 0);
        }
    }

    public function __destruct() {
        $this->clase_db->DB_Desconectar();
        $this->clase_db = null;
    }

    public function tabla_listar_empleados($parametros) {
        $param = $parametros;
        $t_anio = $param["t_anio"];
        $registros = array();
        $str_sql = "SELECT DISTINCT oee.cedula, tm.nombres, tm.apellidos, tu.descrip3 AS unidad, tc.descripcion AS cargo, ud.descripcion AS dotacion, anio 
        FROM uniformes.orden_entrega_empleado oee INNER JOIN uniformes.trabajador_talla tt ON tt.cedula=oee.cedula 
        LEFT JOIN uniformes.detalle_rubros_empleado dre ON oee.id=dre.id_entrega 
        LEFT JOIN tablasmaestras.ubicacion tu ON tt.codunidad=tu.codigo
        LEFT JOIN tablasmaestras.cargos tc ON tt.codcargo=tc.codigo
        LEFT JOIN tablasmaestras.maestra tm ON tt.cedula=tm.cedula
        LEFT JOIN tablasmaestras.tipo_nomina tn ON tt.nomina=tn.codigo 
        LEFT JOIN uniformes.dotaciones ud ON ud.id=oee.dotacion
        LEFT JOIN uniformes.rubro ur ON dre.rubro=ur.id 
        LEFT JOIN uniformes.tallas ut ON dre.talla=ut.id 
        WHERE tn.codigo = 3  AND anio =$t_anio";

        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'cedula' => $reg['cedula'],
                'nombres' => $reg['nombres'],
                'apellidos' => $reg['apellidos'],
                'unidad' => $reg['unidad'],
                'cargo' => $reg['cargo'],
                'dotacion' => $reg['dotacion'],
                'anio' => $reg['anio']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);        
    }

    public function seleccionar_anio($parametros) {
        $registros = array();
        $str_sql = 'SELECT DISTINCT anio FROM uniformes.orden_entrega_empleado ';
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'anio' => $reg['anio']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }

    public function generar_reporte() {
        $str_sql = "SELECT DISTINCT oee.cedula, tm.nombres, tm.apellidos, substr(TRIM(tu.descrip3),0,30) AS unidad, substr(TRIM(tc.descripcion),0,30) AS cargo, TRIM(ud.descripcion) AS dotacion, anio 
        FROM uniformes.orden_entrega_empleado oee INNER JOIN uniformes.trabajador_talla tt ON tt.cedula=oee.cedula 
        LEFT JOIN uniformes.detalle_rubros_empleado dre ON oee.id=dre.id_entrega 
        LEFT JOIN tablasmaestras.ubicacion tu ON tt.codunidad=tu.codigo
        LEFT JOIN tablasmaestras.cargos tc ON tt.codcargo=tc.codigo
        LEFT JOIN tablasmaestras.maestra tm ON tt.cedula=tm.cedula
        LEFT JOIN tablasmaestras.tipo_nomina tn ON tt.nomina=tn.codigo 
        LEFT JOIN uniformes.dotaciones ud ON ud.id=oee.dotacion
        LEFT JOIN uniformes.rubro ur ON dre.rubro=ur.id 
        LEFT JOIN uniformes.tallas ut ON dre.talla=ut.id ";
        $resultado = array();
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);
        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = new stdClass();
            $registro->cedula = $reg["cedula"];
            $registro->nombres = $reg["nombres"];
            $registro->apellidos = $reg["apellidos"];
            $registro->unidad = $reg["unidad"];
            $registro->cargo = $reg["cargo"];
            $registro->dotacion = $reg["dotacion"];
            $registro->anio = $reg["anio"];

            $resultado[] = $registro;
        }
        return $resultado;
    }

    public function seleccionar_orden($parametros) {
        $cedula=$parametros['cedula'];
        $registros = array();
        $str_sql = "SELECT DISTINCT dre.id, ur.rubro, dre.cantidad, ut.talla, ud.descripcion AS dotacion, to_char(fecha1, 'DD-MM-YYYY') as
                    fecha1, to_char(fecha2, 'DD-MM-YYYY') as
                    fecha2, oee.anio FROM uniformes.detalle_rubros_empleado dre
            INNER JOIN uniformes.orden_entrega_empleado oee ON oee.id=dre.id_entrega
            LEFT JOIN uniformes.rubro ur ON dre.rubro=ur.id
            LEFT JOIN tablasmaestras.maestra tme ON oee.cedula=tme.cedula
            LEFT JOIN uniformes.dotaciones ud ON oee.dotacion=ud.id
            LEFT JOIN uniformes.tallas ut ON dre.talla=ut.id
            WHERE oee.cedula=$cedula";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'id' => $reg['id'],
                'rubro' => $reg['rubro'],
                'talla' => $reg['talla'],
                'cantidad' => $reg['cantidad'],
                'dotacion' => $reg['dotacion'],
                'fecha1' => $reg['fecha1'],
                'fecha2' => $reg['fecha2'],
                'anio' => $reg['anio']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }

    public function seleccionar_rubros($parametros){
        $id_tipo_rubro = $parametros['id_tipo_rubro'];
        $registros = array();
        $str_sql = "SELECT id, rubro FROM uniformes.rubro WHERE estado='t' AND id_tipo_rubros =$id_tipo_rubro";

        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);
        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'rubro'=>$reg['rubro'],
                'id'=>$reg['id']          
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }

    public function seleccionar_tipo_rubro($parametros){
        $registros = array();
        $str_sql = "SELECT id, descripcion FROM uniformes.tipo_rubros WHERE estado='t'";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(      
                'descripcion'=>$reg['descripcion'],
                'id'=>$reg['id']          
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    } 


    public function seleccionar_dotacion($parametros) {
        $registros = array();
        $str_sql = 'SELECT id, descripcion FROM uniformes.dotaciones;';
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'id' => $reg['id'],
                'descripcion' => $reg['descripcion']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }

    public function seleccionar_talla($parametros) {
        $registros = array();
        $str_sql = "SELECT id, talla, estado FROM uniformes.tallas WHERE estado= 't'";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'id' => $reg['id'],
                'talla' => $reg['talla'],
                'estado' => $reg['estado']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }

    public function seleccionar_anio_dotacion($parametros) {
        $registros = array();
        $str_sql = 'SELECT DISTINCT aniodotacion FROM uniformes.trabajador_talla';
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'aniodotacion' => $reg['aniodotacion']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }

    public function eliminar_rubros($parametros){
        $id = $this->obtener_id_detalle($parametros);
        $str_sql="DELETE FROM uniformes.detalle_rubros_empleado WHERE id=$id";
        $consulta = $this->bd_rrhh->DB_Modificar_lista($str_sql);
        return $consulta;
    }

    public function eliminar($param) {
        $parametros = $param;
        $registros = array();
        $eliminado = $this->eliminar_rubros($parametros);
            if ($eliminado) {
                $resultado = array('mensaje' => 'eliminado');
            }
        return json_encode($resultado);
    }

    public function actualizar_inventario($parametros){
        $cantidad=$parametros['cantidad'];
        $rubro=$parametros['rubro'];
        $talla=$parametros['talla'];
        $str_sql="UPDATE uniformes.inventario_rubros SET cantidad=cantidad-$cantidad[0] WHERE id_rubro=$rubro[0] AND talla=$talla[0]";
        return $str_sql;
    }

    public function inventario_actualizar($param) {
        $parametros = $param;
        $registros = array();
        $actualizado = $this->actualizar_inventario($parametros);
        $consulta = $this->bd_rrhh->DB_Modificar_lista($actualizado);
            if ($consulta) {
                $resultado = array('mensaje' => 'actualiza');
            }
        return json_encode($resultado);
    }
/*
    public function modificar_encabezado_rubro($parametros) {
        $cedula = $parametros['cedula'];
        $fecha1 = $parametros['fecha1'];
        $fecha2 = $parametros['fecha2'];
        $dotacion = $parametros['dotacion'];
        $anio = $parametros['anio'];

        $tabla = 'uniformes.orden_entrega_empleado';
        $str_sql = "UPDATE $tabla  SET 
                    fecha1 ='$fecha1',
                    fecha2 = '$fecha2',
                    dotacion = $dotacion,
                    anio = $anio
                    WHERE cedula =$cedula";  
                   // echo $str_sql."holaaaa";   

        return $str_sql;
    }

    private function obtener_id_detalle($parametros){
        $cedula =$parametros['cedula'];
        $str_sql="SELECT dre.id FROM uniformes.detalle_rubros_empleado dre INNER JOIN uniformes.orden_entrega_empleado oee ON oee.id=dre.id_entrega WHERE cedula=$cedula";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);
        $reg = $this->bd_rrhh->DB_fetch_array($consulta);
        $id_detalle=$reg['id'];
        //echo $id_detalle."ho";

        return $id_detalle;
    }

    public function modificar_orden_dotacion($parametros) {
 
        $id = $this->obtener_id_detalle($parametros);
        $cantidad = $parametros['cantidad'];
        $rubro = $parametros['rubro'];
        $talla = $parametros['talla'];

        $tabla = 'uniformes.detalle_rubros_empleado';
        $str_sql = "UPDATE $tabla  SET 
                    rubro =$rubro[0],
                    cantidad = $cantidad[0],
                    talla = $talla[0]
                    WHERE id =$id"; 
                    //echo $str_sql."holis";   
        return $str_sql;
    }



    public function actualizar_rubros($param){
        $parametros = $param;
        $modificado = $this->modificar_orden_dotacion($parametros);
        $result = $this->bd_rrhh->DB_Modificar_lista($modificado); 
        if ($result) {
            $resultado = array('mensaje' => 'actualizados');
        }           
        return json_encode($resultado);
    }

    public function actualizar_encabezado($param){
        $parametros = $param;
        $modificado = $this->modificar_encabezado_rubro($parametros);
        $result = $this->bd_rrhh->DB_Modificar_lista($modificado); 
        if ($result) {
            $resultado = array('mensaje' => 'actualizado');
        }           
        return json_encode($resultado);
    }*/

    public function actualizar($param) {
        $parametros = $param;
        $registros = array();
        $insertado = $this->ing_dotacion_cargo($parametros);
        if ($insertado) {
            $resultado = array('mensaje' => 'actualizado');
        }
        return json_encode($resultado);
    }

    public function existe_rubro($rubro, $id_entrega) {
        $existe = FALSE;
        $sql = "SELECT id FROM uniformes.detalle_rubros_empleado WHERE rubro=$rubro AND id_entrega=$id_entrega";
        $resul = $this->bd_rrhh->DB_Consulta($sql); 
        if ($this->bd_rrhh->DB_num_rows($resul) > 0) {

            $existe = TRUE;
        }
        return $existe;
    }

    private function existe_orden($cedula, $fecha1){
        $existe = FALSE;
        $str_sql="SELECT id  FROM uniformes.orden_entrega_empleado WHERE cedula=$cedula  AND fecha1='$fecha1'";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);
        if ($this->bd_rrhh->DB_num_rows($consulta) > 0) {

            $existe = TRUE;
        }
        return $existe;
    }

    private function obtener_id($parametros){
        $cedula =$parametros['cedula'];
        $fecha1 =$parametros['fecha1'];
        $str_sql="SELECT id  FROM uniformes.orden_entrega_empleado WHERE cedula=$cedula  AND fecha1='$fecha1'";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);
        $reg = $this->bd_rrhh->DB_fetch_array($consulta);
        $id_entrega=$reg['id'];
        return $id_entrega;
    }

    public function ing_dotacion_cargo($parametros) {
        $tabla = 'uniformes.detalle_rubros_empleado';
        $rubro =$parametros['rubro'];
        $cantidad =$parametros['cantidad'];
        $talla =$parametros['talla'];
        $cedula =$parametros['cedula'];
        $registrado=FALSE;
        $fecha1=$parametros['fecha1'];
        $orden = $this->existe_orden($cedula,$fecha1);

        if(!$orden){
                           
                $into2 = 'cedula, dotacion, fecha1, fecha2, anio';
                $tabla2= 'uniformes.orden_entrega_empleado';
                $valores =  trim($parametros['cedula']) . ','
                    . trim($parametros['dotacion']) . ','
                    . '\'' . trim($parametros['fecha1']) . '\','
                    . '\'' . trim($parametros['fecha2']) . '\','
                    . trim($parametros['anio']) . '';
                $registrado =$this->bd_rrhh->DB_Insertar($tabla2, $into2, $valores);
             
           // } 
            $id_entrega = $this->obtener_id($parametros);

            if($id_entrega>0){

                $into = 'id_entrega, rubro, cantidad, talla';
                $i=0;
                foreach ($rubro as $clave => $valor) {
                    if (!$this->existe_rubro($valor, $id_entrega)) {
                        $values = '\''.$id_entrega . '\',\'' . $valor . '\',\''. $cantidad[$i] . '\',\'' . $talla[$i] . '\'';
                        $i++;
                        $registrado =$this->bd_rrhh->DB_Insertar($tabla, $into, $values);
                    }
                }
            } 
        }
        return ($registrado) ? TRUE : FALSE;
    }

}
?>