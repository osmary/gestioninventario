<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class proveedor_mod extends CI_Model {

    var $ambiente;
    var $bd_sistema;

    public function __construct() {
        parent::__construct();
        try {
            $this->bd_sistema = new clase_db();
            $this->ambiente = $this->bd_sistema->iniciarConexion('application/models/include/', 'sistema');
            if (!$this->ambiente[0]) {
                throw new Exception($this->ambiente[1]);
            }
          $this->bd_rrhh = new clase_db();
            $this->ambiente_rrhh = $this->bd_rrhh
            ->iniciarConexion('application/models/include/', 'rrhh');

        } catch (Exception $e) {
            error_log('Archivo' . __FILE__ . ' Funcion:' . __FUNCTION__ . ', ' . $e->getMessage(), 0);
        }
    }

    public function __destruct() {            
            $this->clase_db->DB_Desconectar();
            $this->clase_db = null;
    }

    public function tabla_proveedor($parametros){
        $registros = array();
        $str_sql = 'SELECT DISTINCT rif, nombre, telefono, direccion FROM uniformes.proveedor WHERE estado='.'\'t'.'\'';
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'rif' => $reg['rif'],
                'nombre' => $reg['nombre'],
                'telefono' => $reg['telefono'],
                'direccion' => $reg['direccion']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }

    public function seleccionar_orden($parametros) {
        $registros = array();
        $str_sql = 'SELECT DISTINCT rif, nombre, telefono, direccion FROM uniformes.proveedor;';
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'rif' => $reg['rif'],
                'nombre' => $reg['nombre'],
                'telefono' => $reg['telefono'],
                'direccion' => $reg['direccion'] 
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }

    public function eliminar_proveedor($param) {
        $rif = $param;
        $str_sql = $this->actualiza_estado($rif);
        $result = $this->bd_rrhh->DB_Modificar_lista($str_sql);
        if($result){
           $resultado = array('mensaje'=>'se elimino');
        }
        return json_encode($resultado);
    }  

    public function actualizar_proveedor_existente($param){
        $parametros = $param;
        $modificado = $this->modificar_proveedor($parametros);
        $result = $this->bd_rrhh->DB_Modificar_lista($modificado); 
        if ($result) {
            $resultado = array('mensaje' => 'actualizado');
        }           
        return json_encode($resultado);
    }

    public function guardar_dotacion_proveedor($param){
        $parametros = $param;
        $registros = array();
        $existe = $this->verificar_existe($parametros);
        $estado = $this->obtener_estado($parametros);

        if($existe){
            if($estado=='t'){
                $resultado = array('mensaje'=>'existe');   
            }
            if($estado == 'f'){
                $str_sql = $this->actualiza_estado($parametros);
                $result = $this->bd_rrhh->DB_Modificar_lista($str_sql);
                if($result){
                    $resultado = array('mensaje'=>'agregado');
                }
            } 
        }
        else {  
            $insertado = $this->agregar($parametros);   
            if($insertado){
                $resultado = array('mensaje'=>'agregado');
            }
        }
        return json_encode($resultado);
    }

    private function verificar_existe($param){
        $rif = $param['rif'];
        $tabla = 'uniformes.proveedor gip';
        $str_sql = "SELECT COUNT(gip.rif) AS num_reg FROM $tabla WHERE gip.rif='$rif';";

        $result = $this->bd_rrhh->DB_Consulta($str_sql);
        $reg = $this->bd_rrhh->DB_fetch_array($result);
        return ($reg['num_reg']) ? TRUE : FALSE;
    }

    public function actualiza_estado($param) {
        $rif = $param['rif'];
        $tabla = 'uniformes.proveedor';
        $str_sql = "UPDATE $tabla SET estado = NOT estado WHERE rif='$rif';";
        return $str_sql;
    }

    private function obtener_estado($param){
        $nombre= $param['nombre'];
        $rif = $param['rif'];
        $str_sql = "SELECT estado FROM uniformes.proveedor WHERE rif='$rif';";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);
        $reg = $this->bd_rrhh->DB_fetch_array($consulta);
        $regis=$reg['estado'];
        return $regis;
    }

    public function agregar($parametros){
        $tabla= 'uniformes.proveedor';
        $into= 'rif, nombre, telefono, direccion';

        $valores = '\'' .trim($parametros['rif']) .'\','
        .'\'' .trim($parametros['nombre']) .'\','
        .'\'' .trim($parametros['telefono']) .'\','
        .'\'' .trim($parametros['direccion']) .'\'';
        
       $registrado = $this->bd_rrhh->DB_Insertar($tabla, $into, $valores);
        return ($registrado) ? TRUE : FALSE;
    } 


    public function modificar_proveedor($parametros) {
        $rif = $parametros['rif'];
        $nombre = $parametros['nombre'];
        $telefono = $parametros['telefono'];
        $direccion = $parametros['direccion'];

        $tabla = 'uniformes.proveedor';
        $str_sql = "UPDATE $tabla SET 
                    nombre ='$nombre',
                    telefono ='$telefono',
                    direccion ='$direccion'
                    WHERE rif ='$rif';";
        return $str_sql;
    }
/*

    'SELECT num_factura, nombre, rif, direccion, telefono, cantidad, rubro, preciounitario, total, fecha_recibido
  FROM gestion_inventario.factura gif INNER JOIN gestion_inventario.proveedor gip ON id_proveedor=rif
  LEFT JOIN gestion_inventario.detalle gid ON gif.id_detalle=gid.id_detalle
  LEFT JOIN uniformes.rubro ur ON tipo_rubro=id'
*/
}
?>