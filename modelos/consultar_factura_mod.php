<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class consultar_factura_mod extends CI_Model {

    var $ambiente;
    var $bd_sistema;

    public function __construct() {
        parent::__construct();
        try {
            $this->bd_sistema = new clase_db();
            $this->ambiente = $this->bd_sistema->iniciarConexion('application/models/include/', 'sistema');
            if (!$this->ambiente[0]) {
                throw new Exception($this->ambiente[1]);
            }
          $this->bd_rrhh = new clase_db();
            $this->ambiente_rrhh = $this->bd_rrhh
            ->iniciarConexion('application/models/include/', 'rrhh');

        } catch (Exception $e) {
            error_log('Archivo' . __FILE__ . ' Funcion:' . __FUNCTION__ . ', ' . $e->getMessage(), 0);
        }
    }

    public function __destruct() {            
            $this->clase_db->DB_Desconectar();
            $this->clase_db = null;
    }

    public function mostrar_factura($param){
        $registros = array();
        $t_proveedor = $param["t_proveedor"];
        $str_sql = "SELECT id_factura, num_factura, rif, fecha_recibido
                    FROM uniformes.factura 
                    INNER JOIN uniformes.proveedor ON id_proveedor=rif 
                    WHERE rif= '$t_proveedor'";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);
        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'id_factura'=>$reg['id_factura'],
                'num_factura'=>$reg['num_factura'],
                'rif'=>$reg['rif'],
                'fecha_recibido'=>$reg['fecha_recibido']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }


    public function generar_reporte($parametros){
        $id_factura= $parametros['id_factura'];
        $registros = array();
        $str_sql = "SELECT num_factura, nombre, rif, direccion, telefono, rubro, cantidad,  preciounitario, total, to_char(fecha_recibido, 'DD-MM-YYYY') AS fecha_recibido
            FROM uniformes.detalle_factura gid 
            INNER JOIN uniformes.factura gif ON gif.id_factura=gid.id_factura
            LEFT JOIN uniformes.rubro ur ON tipo_rubro=id
            LEFT JOIN uniformes.proveedor gip ON id_proveedor=rif 
            WHERE gif.id_factura=$id_factura";
        $resultado= array();
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);
        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro= new stdClass();
            $registro->num_factura=$reg["num_factura"];
            $registro->nombre=$reg["nombre"];
            $registro->rif=$reg["rif"];
            $registro->direccion=$reg["direccion"];
            $registro->telefono=$reg["telefono"];
            $registro->rubro=$reg["rubro"];
            $registro->cantidad=$reg["cantidad"];
            $registro->preciounitario=$reg["preciounitario"];
            $registro->total=$reg["total"];
            $registro->fecha_recibido=$reg["fecha_recibido"];
            $resultado[]=$registro;
        }
        return $resultado;
    }

    public function seleccionar_proveedor($parametros){
        $registros = array();
        $str_sql = 'SELECT rif, nombre FROM uniformes.proveedor;';
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'rif' => $reg['rif'],
                'nombre' => $reg['nombre']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }


}
?>