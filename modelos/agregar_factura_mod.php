<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class agregar_factura_mod extends CI_Model {

    var $ambiente;
    var $bd_sistema;

    public function __construct() {
        parent::__construct();
        try {
            $this->bd_sistema = new clase_db();
            $this->ambiente = $this->bd_sistema->iniciarConexion('application/models/include/', 'sistema');
            if (!$this->ambiente[0]) {
                throw new Exception($this->ambiente[1]);
            }
          $this->bd_rrhh = new clase_db();
            $this->ambiente_rrhh = $this->bd_rrhh
            ->iniciarConexion('application/models/include/', 'rrhh');

        } catch (Exception $e) {
            error_log('Archivo' . __FILE__ . ' Funcion:' . __FUNCTION__ . ', ' . $e->getMessage(), 0);
        }
    }

    public function __destruct() {            
            $this->clase_db->DB_Desconectar();
            $this->clase_db = null;
    }
/////////////// funcion para seleccionar el proveedor segun lo que este en la base de datos////
    public function seleccionar_proveedor($parametros){
        $registros = array();
        $str_sql = 'SELECT rif FROM uniformes.proveedor;';
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'rif' => $reg['rif']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }
/////////////// funcion para seleccionar los rubros segun lo que este en la base de datos////

    public function seleccionar_rubros($parametros){
        $registros = array();
        $str_sql = "SELECT id, rubro FROM uniformes.rubro WHERE estado='t'";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'id'=>$reg['id'],
                'rubro'=>$reg['rubro']          
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }
// funcion para obtener el id de la factura para ingresarla en el detalle//

    private function obtener_id($parametros){
        $num_factura =$parametros['num_factura'];
        $id_proveedor =$parametros['id_proveedor'];
        $str_sql="SELECT id_factura  FROM uniformes.factura WHERE num_factura=$num_factura  AND id_proveedor=$id_proveedor";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);
        $reg = $this->bd_rrhh->DB_fetch_array($consulta);
        $id_factura=$reg['id_factura'];
        return $id_factura;
    }

// funcion para saber si ese detalle existe con ese id_factura y con el tipo de rubro si no existe ingresa//

    function existe($id_factura, $tipo_rubro) {
        $existe = FALSE;
        $sql = "SELECT id_detalle FROM uniformes.detalle_factura WHERE id_factura=$id_factura AND tipo_rubro=$tipo_rubro";
        $resul = $this->bd_rrhh->DB_Consulta($sql);
        if ($this->bd_rrhh->DB_num_rows($resul) > 0) {

            $existe = TRUE;
        }
        return $existe;
    }
// funcion para insertar la factura, primero el encabezado si no existe ese id y luego el detalle de la factura//

    public function insertar_factura($parametros) {
        $tabla = 'uniformes.detalle_factura';
        $rubro =$parametros['tipo_rubro'];
        $cantidad =$parametros['cantidad'];
        $precio =$parametros['preciounitario'];
        $total =$parametros['total'];
        $id_factura =$parametros['id_factura'];
        $registrado=FALSE;

        if($id_factura==0){
            $into2 = 'num_factura, fecha_recibido, id_proveedor';
            $tabla2= 'uniformes.factura';
            $valores = trim($parametros['num_factura']) .','
                . '\'' . trim($parametros['fecha_recibido']) . '\','
                . trim($parametros['id_proveedor']) .'';
                
            $registrado =$this->bd_rrhh->DB_Insertar($tabla2, $into2, $valores);

            $id_factura = $this->obtener_id($parametros);

            if($id_factura>0){
                $into = 'id_factura, cantidad, tipo_rubro, preciounitario, total';
                $i=0;
                foreach ($rubro as $clave => $valor) {
                    if (!$this->existe($id_factura,$valor)) {
                        $values = '\''.$id_factura . '\',\'' . $cantidad[$i] .'\',\'' . $valor .'\',\'' . $precio[$i] . '\',\'' . $total[$i] .'\'';
                        $i++;
                        $registrado =$this->bd_rrhh->DB_Insertar($tabla, $into, $values);
                    }
                } 
            }
        }
        return ($registrado) ? TRUE : FALSE;

    }
///funcion que verifica si existe esa factura, y si es asi envia un mensaje que existe y no agrega
    private function verificar_existe($param){
        $id_proveedor = $param['id_proveedor'];
        $num_factura = $param['num_factura'];
        $tabla = 'uniformes.factura';
        $str_sql = "SELECT COUNT(id_proveedor) AS area FROM uniformes.factura WHERE id_proveedor=$id_proveedor AND num_factura=$num_factura;";
        $result = $this->bd_rrhh->DB_Consulta($str_sql);
        $reg = $this->bd_rrhh->DB_fetch_array($result);
        return ($reg['area']) ? TRUE : FALSE;
    }
//funcion para guardar
    public function guardar_factura($param){
        $parametros = $param;
        $registros = array();
        $existe = $this->verificar_existe($parametros);

        if($existe){
                $resultado = array('mensaje'=>'existe');   
        }else {  
            $insertado = $this->insertar_factura($parametros);   
            if($insertado){
                $resultado = array('mensaje'=>'agregado');
            }
        }
        return json_encode($resultado);
    }
}
?>