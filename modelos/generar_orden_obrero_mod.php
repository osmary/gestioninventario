<?php
 
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class generar_orden_obrero_mod extends CI_Model {

    var $ambiente;
    var $bd_sistema;

    public function __construct() {
        parent::__construct();
        try {
            $this->bd_sistema = new clase_db();
            $this->ambiente = $this->bd_sistema->iniciarConexion('application/models/include/', 'sistema');
            if (!$this->ambiente[0]) {
                throw new Exception($this->ambiente[1]);
            }
 
            $this->bd_rrhh = new clase_db();
            $this->ambiente_rrhh = $this->bd_rrhh->iniciarConexion('application/models/include/', 'rrhh');
           
        } catch (Exception $e) {
            error_log('Archivo' . __FILE__ . ' Funcion:' . __FUNCTION__ . ', ' . $e->getMessage(), 0);
        }
    }

    public function __destruct() {            
        $this->clase_db->DB_Desconectar();
        $this->clase_db = null;
    }

  
    public function tabla_listar_obreros($parametros){
        $param = $parametros;
        $t_nomina = $param["t_nomina"];
        $t_anio = $param["t_anio"];
        $t_unidad = $param["t_unidad"];
        $and='';
        $registros = array();
        if ($t_unidad!=='Seleccione una opcion' && !empty($t_unidad)){
            $and= " AND tu.descrip3 ='$t_unidad';";
        } 
        $registros = array();

        $str_sql = "SELECT DISTINCT ug.descripcion AS ubicacion, tu.descrip3 AS unidad, tm.nombres, tm.apellidos, tm.cedula, tm.fec_ing_obr AS fechaing, tc.descripcion AS cargo, tt.aniodotacion AS dotacion, tm.sexo, tn.descripcion AS nomina, tt.calzado, tt.pantalon, tt.camisa, tt.chaqueta
                FROM ((((((uniformes.trabajador_talla tt INNER JOIN tablasobrero.maestra1 mst ON tt.cedula=mst.cedula)
                LEFT JOIN tablasmaestras.ubicacion tu ON tt.codunidad=tu.codigo)
                LEFT JOIN tablasobrero.cargos tc ON tt.codcargo=tc.codigo)
                LEFT JOIN tablasmaestras.tipo_nomina tn ON tt.nomina=tn.codigo)
                LEFT JOIN tablasobrero.maestra tm ON tm.cedula=mst.cedula)
                LEFT JOIN tablasmaestras.ubicacion_geografica ug ON ug.cod_ubicaciongeo=tm.ubic_geograf)
                WHERE tn.codigo =$t_nomina  AND tt.aniodotacion =$t_anio  $and";

            $consulta = $this->bd_rrhh->DB_Consulta($str_sql);
          
        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'ubicacion' => $reg['ubicacion'],
                'unidad' => $reg['unidad'],
                'nombres' => $reg['nombres'],
                'apellidos' => $reg['apellidos'],
                'cedula' => $reg['cedula'],
                'fechaing' => $reg['fechaing'],
                'cargo' => $reg['cargo'],
                'dotacion' => $reg['dotacion'],
                'sexo' => $reg['sexo'],  
                'nomina' => $reg['nomina'],       
                'calzado' => $reg['calzado'],
                'pantalon' => $reg['pantalon'],
                'camisa' => $reg['camisa'],
                'chaqueta' => $reg['chaqueta']

            );

            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }

    

    public function clausulas_con_relacion($parametros){
        $registros = array();
        $str_sql = 'SELECT DISTINCT aniodotacion FROM uniformes.trabajador_talla';
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'aniodotacion'=>$reg['aniodotacion']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }

    public function seleccionar_rubros($parametros){
        $id_tipo_rubro = $parametros['id_tipo_rubro'];
        $registros = array();
        $str_sql = "SELECT id, rubro FROM uniformes.rubro WHERE estado='t' AND id_tipo_rubros =$id_tipo_rubro";

        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);
        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'rubro'=>$reg['rubro'],
                'id'=>$reg['id']          
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }

    public function seleccionar_tipo_rubro($parametros){
        $registros = array();
        $str_sql = "SELECT id, descripcion FROM uniformes.tipo_rubros WHERE estado='t'";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(      
                'descripcion'=>$reg['descripcion'],
                'id'=>$reg['id']          
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    } 

    public function seleccionar_unidad($parametros){
        $param = $parametros;
        $anno = $param['anno'];
        $t_nomina = $param['t_nomina'];
        $registros = array();
        $str_sql = "SELECT DISTINCT tu.codigo, tu.descrip3 AS unidad
            FROM (((uniformes.trabajador_talla tt INNER JOIN tablasobrero.maestra1 mst ON tt.cedula=mst.cedula)
            LEFT JOIN tablasmaestras.ubicacion tu ON tt.codunidad=tu.codigo)
            LEFT JOIN tablasmaestras.tipo_nomina tn ON tt.nomina=tn.codigo)
            WHERE tn.codigo =$t_nomina AND tt.aniodotacion =$anno";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'codigo' => $reg['codigo'],
                'unidad'=>$reg['unidad']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
        print_r($resultado);

    }
     public function seleccionar_anio($parametros){
        $registros = array();
        $str_sql = 'SELECT DISTINCT aniodotacion FROM uniformes.trabajador_talla';
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);
        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'aniodotacion'=>$reg['aniodotacion']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }

    public function seleccionar_dotacion($parametros) {
        $registros = array();
        $str_sql = 'SELECT id, descripcion FROM uniformes.dotaciones;';
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'id' => $reg['id'],
                'descripcion' => $reg['descripcion']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }
    
    public function seleccionar_orden($parametros){
        $cedula=$parametros['cedula'];
        $registros = array();
        $str_sql = "SELECT DISTINCT dro.id, ur.rubro, dro.cantidad, ut.talla, ud.descripcion AS dotacion, to_char(fecha1, 'DD-MM-YYYY') as
                    fecha1, to_char(fecha2, 'DD-MM-YYYY') as
                    fecha2, oeo.anio FROM uniformes.detalle_rubros_obrero dro
            INNER JOIN uniformes.orden_entrega_obrero oeo ON oeo.id=dro.id_entrega
            LEFT JOIN uniformes.rubro ur ON dro.rubro=ur.id
            LEFT JOIN tablasobrero.maestra tm ON oeo.cedula=tm.cedula
            LEFT JOIN uniformes.dotaciones ud ON oeo.dotacion=ud.id
            LEFT JOIN uniformes.tallas ut ON dro.talla=ut.id
            WHERE oeo.cedula=$cedula";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'id' => $reg['id'],
                'rubro' => $reg['rubro'],
                'talla' => $reg['talla'],
                'cantidad' => $reg['cantidad'],
                'dotacion' => $reg['dotacion'],
                'fecha1' => $reg['fecha1'],
                'fecha2' => $reg['fecha2'],
                'anio' => $reg['anio']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }

    public function seleccionar_talla($parametros) {
        $registros = array();
        $str_sql = "SELECT id, talla, estado FROM uniformes.tallas WHERE estado= 't'";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);

        while ($reg = $this->bd_rrhh->DB_fetch_array($consulta)) {
            $registro = array(
                'id' => $reg['id'],
                'talla' => $reg['talla'],
                'estado' => $reg['estado']
            );
            array_push($registros, $registro);
        }
        $resultado['registros'] = $registros;
        return json_encode($resultado);
    }

    public function actualizar_inventario($parametros){
        $cantidad=$parametros['cantidad'];
        $rubro=$parametros['rubro'];
        $talla=$parametros['talla'];
        $str_sql="UPDATE uniformes.inventario_rubros SET cantidad=cantidad-$cantidad[0] WHERE id_rubro=$rubro[0] AND talla=$talla[0]";
        return $str_sql;
    }

    public function inventario_actualizar($param) {
        $parametros = $param;
        $registros = array();
        $actualizado = $this->actualizar_inventario($parametros);
        $consulta = $this->bd_rrhh->DB_Modificar_lista($actualizado);
            if ($consulta) {
                $resultado = array('mensaje' => 'actualiza');
            }
        return json_encode($resultado);
    }

    public function guardar_dotacion_cargo_obrero($param) {
        $parametros = $param;
        $registros = array();

        $existe = $this->verificar_dotacion_cargo($parametros);
        $estado = $this->obtener_est_dotacion_cargo($parametros);

        if ($existe) {
            if ($estado == 'f') {
                $str_sql = $this->actualiza_dotacion_cargo($parametros);
                $result = $this->bd_rrhh->DB_Modificar_lista($str_sql);

                if ($result) {
                    $resultado = array('mensaje' => 'agregado');
                }
            }
        } else {
            $insertado = $this->ing_dotacion_cargo($parametros);
            if ($insertado) {
                $resultado = array('mensaje' => 'agregado');
            }
        }
        return json_encode($resultado);
    }

    private function verificar_dotacion_cargo($param){
        $cedula = $param['cedula'];
        $tabla = 'uniformes.orden_entrega_obrero tp';
        $str_sql = "SELECT COUNT(tp.cedula) AS area FROM $tabla WHERE tp.cedula =$cedula";
        $result = $this->bd_rrhh->DB_Consulta($str_sql);
        $reg = $this->bd_rrhh->DB_fetch_array($result);
        return ($reg['area']) ? TRUE : FALSE;
    }

    public function actualiza_dotacion_cargo($param) {
        $cedula = $param['cedula'];
        $tabla = 'uniformes.orden_entrega_obrero';
        $str_sql = "UPDATE $tabla SET estado = NOT estado WHERE cedula =$cedula";
        return $str_sql;
    }

    private function obtener_est_dotacion_cargo($param){
        $cedula = $param['cedula'];
        $str_sql = "SELECT estado FROM uniformes.orden_entrega_obrero WHERE cedula =$cedula";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);
        $reg = $this->bd_rrhh->DB_fetch_array($consulta);
        $regis=$reg['estado'];
        return $regis;
    }

    private function obtener_id($parametros){
        $cedula =$parametros['cedula'];
        $fecha1 =$parametros['fecha1'];
        $str_sql="SELECT id  FROM uniformes.orden_entrega_obrero WHERE cedula=$cedula  AND fecha1='$fecha1'";
        $consulta = $this->bd_rrhh->DB_Consulta($str_sql);
        $reg = $this->bd_rrhh->DB_fetch_array($consulta);
        $id_entrega=$reg['id'];
        return $id_entrega;
    }

    function existe($rubro, $id_entrega) {
        $existe = FALSE;
        $sql = "SELECT id FROM uniformes.detalle_rubros_obrero WHERE rubro=$rubro AND id_entrega=$id_entrega";
        $resul = $this->bd_rrhh->DB_Consulta($sql); 
        if ($this->bd_rrhh->DB_num_rows($resul) > 0) {

            $existe = TRUE;
        }
        return $existe;
    }

public function ing_dotacion_cargo($parametros) {
        $tabla = 'uniformes.detalle_rubros_obrero';
        $rubro =$parametros['rubro'];
        $talla =$parametros['talla'];
        $cantidad =$parametros['cantidad'];
        $id_entrega =$parametros['id_entrega'];
        $registrado=FALSE;
        $fecha=$parametros['fecha2'];
        $fecha2='';

        if($id_entrega==0){
            if ($fecha!=='fecha 2' && !empty($fecha)){
                $fecha2= $parametros['fecha2'];
            } 
            //$fecha = $this->obtener_fecha($parametros);
            //if ($fecha>0 || empty($fecha)){
                           
                $into2 = 'cedula, dotacion, fecha1, fecha2, anio';
                $tabla2= 'uniformes.orden_entrega_obrero';
                $valores =  trim($parametros['cedula']) . ','
                    . trim($parametros['dotacion']) . ','
                    . '\'' . trim($parametros['fecha1']) . '\','
                    . '\'' . trim($parametros['fecha2']) . '\','
                    . trim($parametros['anio']) . '';
                $registrado =$this->bd_rrhh->DB_Insertar($tabla2, $into2, $valores);
             
           // } 
            $id_entrega = $this->obtener_id($parametros);

            if($id_entrega>0){

                $into = 'id_entrega, rubro, cantidad, talla';
                $i=0;
                foreach ($rubro as $clave => $valor) {
                    if (!$this->existe($valor, $id_entrega)) {
                        $values = '\''.$id_entrega . '\',\'' . $valor . '\',\''. $cantidad[$i] . '\',\'' . $talla[$i] . '\'';
                        $i++;
                        $registrado =$this->bd_rrhh->DB_Insertar($tabla, $into, $values);
                    }
                }
            } 
        }
        return ($registrado) ? TRUE : FALSE;

    }

}