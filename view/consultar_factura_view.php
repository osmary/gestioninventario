<div class="bg-container" style="margin-bottom: 1em;">
	<strong><h3 class="cintillo">CONSULTAR FACTURA</h3></strong>&nbsp;&nbsp;
	<div id="mensaje" style="text-align:center; display: none;"><h4></h4></div>
	<br><br>
	<div class="form-horizontal">
	    <div class="form-group">
		    <label for="inputEmail3" class="col-sm-1 control-label">Proveedor:</label>
			<div class="col-sm-3">
				<select class="form-control form-group" id="proveedor">
					<option>Seleccione Proveedor</option>
				</select>
				<input type="hidden" id="id_factura">
			</div>
		</div>
	</div>
	<div id="div_mostrar">						
		<table id="tabla_factura" class="table table-hover" width="100%">
			<thead>
				<tr class="cintillo">
					<th>Codigo</th>
					<th>Rif</th>
					<th>Numero de factura</th>
					<th>Fecha Recibido</th>
					<th>Accion</th> 
				</tr>
			</thead>	
		</table>				
	</div>
</div>