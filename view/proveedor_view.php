<div class="bg-container" style="margin-bottom: 1em;">
	<strong><h3 class="cintillo">AGREGAR PROVEEDOR</h3></strong>&nbsp;&nbsp;
	<div id="mensaje" style="text-align:center; display: none;"><h4></h4></div>
	<br><br> 
    <div class="row">  
		<div class="col-md-4" id="formulario">
			<br>
			<h4 class="text-center"><b>Nuevo Proveedor</b></h4>
			<br>
			<div class="form-horizontal">
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Rif:</label>
					<div class="col-sm-8">
						<input class="form-control form-group" type="text" id="rif" placeholder="Rif" required>
					</div>
				</div>
			</div>
			<div class="form-horizontal">
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Proveedor:</label>
					<div class="col-sm-8">
						<input class="form-control form-group" type="text" id="proveedor" placeholder="Proveedor" required>
					</div>
				</div>
			</div>
			<div class="form-horizontal">
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Direccion:</label>
					<div class="col-sm-8">
						<input class="form-control form-group" type="text" id="direccion" placeholder="Direccion" required>
					</div>
				</div>
			</div>
			<div class="form-horizontal">
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Telefono:</label>
					<div class="col-sm-8">
						<input class="form-control form-group" type="text" id="telefono" placeholder="Telefono" required>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-md-offset-5">
				<br>
				<button class="btn btn-primary" type="button" id="guardar">Guardar</button>
			</div>
		</div>
		<div class="col-md-8">
			<div id="div_proveedores">						
				<table id="tabla_de_proveedores" class="table table-hover" width="100%">
					<thead>
						<tr class="cintillo">
							<th>Rif</th>
							<th>Nombre</th>
							<th>Telefono</th>
							<th>Direccion</th>
							<th>Accion</th>
						</tr>
					</thead>
				</table>				
			</div>
		</div>
	</div>
</div>
<!-- Modal --> 
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->	
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4>Informacion Proveedor</h4>
			</div>
			<div id="area_orden" class="modal-body">
				<h4 id="title_por_entregar" class="bold modal-title"></h4><br><br>
				<div class="row">
					<div class="col-md-6">
						<div class="form-horizontal">
						  	<div class="form-group">
						    	<label for="inputEmail3" class="col-md-5 control-label">Rif: </label>
						    	<div class="col-md-6">
						    		<input id="modal-rif" type="text" class="form-control" placeholder="Rif">
						   		</div>
						  	</div>
						</div>
						<div class="form-horizontal">
						  	<div class="form-group">
						    	<label for="inputEmail3" class="col-md-5 control-label">Nombre: </label>
						    	<div class="col-md-6">
						    		<input id="modal-proveedor" type="text" class="form-control" placeholder="Nombre proveedor">
						   		</div>
						  	</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-horizontal">
						  	<div class="form-group">
						    	<label for="inputEmail3" class="col-md-5 control-label">Telefono: </label>
						    	<div class="col-md-6">
						    		<input id="modal-telefono" type="text" class="form-control" placeholder="Telefono">
						   		</div>
						  	</div>
						</div>
						<div class="form-horizontal">
						  	<div class="form-group">
						    	<label for="inputEmail3" class="col-md-5 control-label">Direccion: </label>
						    	<div class="col-md-6">
						    		<input id="modal-direccion" type="text" class="form-control" placeholder="Direccion">
						   		</div>
						  	</div>
						</div>	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" id="eliminar_proveedor">Eliminar</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary" id="actualizar_proveedor">Guardar</button>
				</div>
			</div>
		</div>
	</div>
</div>