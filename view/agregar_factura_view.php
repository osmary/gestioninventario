<div class="bg-container" style="margin-bottom: 1em;">
	<strong><h3 class="cintillo">AGREGAR FACTURA</h3></strong>&nbsp;&nbsp;
	<div id="mensaje" style="text-align:center; display: none;"><h4></h4></div>
	<br><br>

	<div class="row" id="formulario">
		<br>

		<div class="form-horizontal">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Numero Factura:</label>
				<div class="col-sm-4">
					<input class="form-control form-group" type="text" id="numero_factura" placeholder="Numero Factura" required>
					<input type="hidden" id="id_factura">
	 			</div>
			</div>
		</div>
		<div class="form-horizontal">
			<div class="form-group">
				<strong><label class="col-sm-3 control-label" width='25%'>Proveedor:</label></strong>
				<div class="col-sm-4">
					<select class="form-control form-group" id="proveedor">
						<option>Seleccione Proveedor</option>
					</select>	
				</div>
			</div>
		</div>
		<div class="form-horizontal">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label" >Fecha recibido:</label>
				<div class="col-sm-4">
					<input id="fecha" type="date" class="form-control form-group" placeholder="fecha">
				</div>
			</div>
		</div>
	</div>
	<div class="row" id="div">
	  <div class="col-sm-3"><strong> Tipo de rubro</strong></div>
	  <div class="col-sm-2"><strong>Cantidad</strong></div>
	  <div class="col-sm-2"><strong>Precio</strong></div>
	  <div class="col-sm-2"><strong>Total</strong></div>
	  <div class="col-sm-1"></div>
	  
	</div>

	<div class="row" id="div_1" >
	  <div class="col-sm-3">
	  		<select class="form-control form-group rubro" name="rubro[]" id="rubro_1" >
				<option>Tipo de rubro</option>
			</select></div>
	  <div class="col-sm-2">
	        <input type="text" name="cantidad[]" id='cantidad_1'  placeholder="Cantidad" class="ver" />
	  </div>
	  <div class="col-sm-2">
	        <input type="text" name="precio[]" id='precio_1' placeholder="precio unitario" class="ver" />
	  </div>
	  <div class="col-sm-2">
	        <input type="text" class="total" name="total[]" id='total_1' placeholder="precio total"/> 	
	  </div>
	  <div class="col-sm-1">
            <input class="btn btn-primary añadir" id="1" type="button" value="+" />
	  </div>  
	</div>


	<button class="btn btn-primary glyphicon glyphicon-floppy-save" type="button" id="guardar">Guardar</button>
</div>