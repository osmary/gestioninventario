<div class="bg-container" style="margin-bottom: 1em;">
	<strong><h3 class="cintillo">ORDEN DE ENTREGA EMPLEADO</h3></strong>
	<br><br>
	<div class="form-horizontal">
	  	<div class="form-group">
		    <label for="inputEmail3" class="col-sm-1 control-label">Año:</label>
		    <div class="col-sm-2">
		    	<select class="form-control" id="clausula"> 		
		      	</select>
		    </div>
		</div>
	    <div class="form-group">
		    <label for="inputEmail3" class="col-sm-1 control-label">Unidad:</label>
				<div class="col-sm-2">
					<select class="form-control" id="unidad"></select>
				</div>
			<button type="button" class="btn btn-primary" id="btn_seleccionar">Buscar</button>
		</div>
	</div> 
	<br>
	<div id="div_mostrar">						
		<table id="tabla_empleado" class="table table-hover" width="100%">
			<thead>
				<tr class="cintillo">
					<th>ubicacion</th>
					<th>unidad</th>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th>cedula</th>
					<th>fechaIng</th>
					<th>cargo</th>
					<th>dotacion</th>
					<th>sexo</th>
					<th>nomina</th>
					<th>calzado</th>
					<th>pantalon</th>
					<th>camisa</th>
					<th>chaqueta</th>
					<th>Accion</th>
				</tr>
			</thead>
		</table>				
	</div>
</div>
 
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->	
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4>DOTACION A ENTREGAR</h4>
			</div>
			<div id="area_orden" class="modal-body">
				<h4 id="title_por_entregar" class="bold modal-title"></h4><br><br>
				<input id="id_entrega" type="hidden">
				<input id="cedula" type="hidden">
				<div class="row" id="div">
					<div class="col-sm-2"><strong> Tipo de rubro</strong></div>
					<div class="col-sm-2"><strong>Rubro</strong></div>
					<div class="col-sm-2"><strong>Talla</strong></div>
					<div class="col-sm-2"><strong>Cantidad</strong></div>
					<div class="col-sm-2"></div>
				</div>

				<div class="row" id="div_1" >
					<div class="col-sm-2">
						<select class="form-control form-group tipo_rubro" name="tiporubro[]" id="tiporubro_1" >
							<option>Tipo de rubro</option>
						</select>
					</div>
					<div class="col-sm-2">
						<select class="form-control form-group rubro" name="rubro[]" id="rubro_1" >
							<option>Rubro</option>
						</select>
					</div>
					<div class="col-sm-2">
				        <select class="form-control form-group talla" name="talla[]" id="talla_1" >
							<option>Talla</option>
						</select>
			  		</div>
					<div class="col-sm-2">
						<input type="text" name="cantidad[]" id='cantidad_1' class="cantidad" placeholder="Cantidad" />
					</div>
					<div class="col-sm-2">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-primary añadir" id="1" type="button" value="+" />
					</div>   
				</div>
				<br>
				<div class="row">
					<div class="col-md-6">
						<div class="form-horizontal">
							<div class="form-group">
								<label for="inputEmail3" class="col-md-3 control-label">Año:</label>
								<div class="col-md-8">
									<select class="form-control" id="anio">
										<option>Seleccione año</option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-horizontal">
							<div class="form-group">
			               		<label class="col-md-3 control-label">Dotacion:</label>
			               		<div class="col-md-8">
			                        <select class="form-control" id="dotacion">
				                        <option  >Seleccione Dotacion</option>
			                        </select>
		                        </div>
			                </div>
			            </div>
	                </div>
	                <div class="col-md-6">
						<div class="form-horizontal">
						  	<div class="form-group">
						    	<label for="inputEmail3" class="col-md-3 control-label">Fecha 1:</label>
						    	<div class="col-md-8">
						    		<input id="fecha_a" type="date" class="form-control" placeholder="fecha 1">
						   		</div>
						  	</div>
						</div>
						<div class="form-horizontal">
						  	<div class="form-group">
						    	<label for="inputEmail3" class="col-md-3 control-label">Fecha 2:</label>
						    	<div class="col-md-8">
						    		<input id="fecha_b" type="date" class="form-control" placeholder="fecha 2">
						   		</div>
						  	</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="guardar_orden_entrega">Guardar Nuevo</button>
			</div>
		</div>
	</div>
</div>

