<?php
  
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class actualizar_orden_obrero_con extends CI_Controller {
    var $modulos = '';
    function __construct() {
        parent::__construct();
        $this->load->model('Utilidades_db_mod');
        $this->load->helper('url');
        $this->load->library('funciones');
        $this->load->library('Pdf');
        $this->load->helper('form', 'url');
        $this->load->helper('js_css');
        $this->cargar_config();
        $this->funciones->verifica_sesion($this->Utilidades_db_mod->ambiente[0], FRONTEND, $this->modulos);
    }
 
    private function cargar_config() {
        //Parametros comunes en todos los metodos
        $this->data['ambiente'] = $this->Utilidades_db_mod->ambiente[1];
        $this->data['titulo_pagina'] = $this->config->item('nombre_sistema');
        $this->data['contenido'] = 'login_view';
        $this->data['mensaje'] = '';
        $this->data['ruta_js'] = $this->config->item('ruta_js');
        $this->data['ruta_css'] = $this->config->item('ruta_css');
        $this->data['ruta_js_sist'] = $this->config->item('ruta_js_sist');
        $this->data['ruta_css_sist'] = $this->config->item('ruta_css_sist');
        $this->data['css_sist'] = array('contenido.css', 'top_view.css', 'principal_view.css');
        $this->data['js_sist'] = array('top_view.js', 'funciones.js', 'actualizar_orden_obrero_view.js');
        $this->data['salir'] = $this->config->item('logoff');
        $this->data['nombre_usuario'] = '';
    }

    function index() {
        $this->data['mostrar_cintillo'] = true;
        $this->data['mostrar_menu'] = 'principal';
        $this->data['menu_activo'] = '';
        if ($this->data['mostrar_menu']) {
            $this->load->model('Top_mod');
            $json_menu = json_decode($this->Top_mod->traer_menu());
            $menu_depurado[$this->data['mostrar_menu']] = $this->funciones->configurar_menu($json_menu, $this->data['mostrar_menu']);
            $this->data['menu_todos'] = json_encode($menu_depurado);
        }
        $this->data['title'] = "Pagina Principal";
        $this->data['contenido'] = 'actualizar_orden_obrero_view';
        $this->data['mensaje'] = '';
        $this->data['nombre_usuario'] = $_SESSION['login'];
        $this->load->view('plantillas/plantilla_view', $this->data);
    }

    public function __destruct() {
        $this->Utilidades_db_mod = null;
        $this->data = null;
    }

    public function tabla_listar_obreros() {
        $parametros = $this->input->post();
        $this->load->model('actualizar_orden_obrero_mod');
        $this->data['datos_tmp'] = $this->actualizar_orden_obrero_mod->tabla_listar_obreros($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }

    public function seleccionar_anios() {
        $parametros = $this->input->post();
        $this->load->model('actualizar_orden_obrero_mod');
        $this->data['datos_tmp'] = $this->actualizar_orden_obrero_mod->seleccionar_anio($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }

    public function seleccionar_rubros() {
        $parametros = $this->input->post();
        $this->load->model('actualizar_orden_obrero_mod');
        $this->data['datos_tmp'] = $this->actualizar_orden_obrero_mod->seleccionar_rubros($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }

    public function seleccionar_tipo_rubro() {
        $parametros = $this->input->post();
        $this->load->model('actualizar_orden_obrero_mod');
        $this->data['datos_tmp'] = $this->actualizar_orden_obrero_mod->seleccionar_tipo_rubro($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }


    public function seleccionar_dotacion() {
        $parametros = $this->input->post();
        $this->load->model('actualizar_orden_obrero_mod');
        $this->data['datos_tmp'] = $this->actualizar_orden_obrero_mod->seleccionar_dotacion($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }

    public function seleccionar_talla() {
        $parametros = $this->input->post();
        $this->load->model('actualizar_orden_obrero_mod');
        $this->data['datos_tmp'] = $this->actualizar_orden_obrero_mod->seleccionar_talla($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }

    public function seleccionar_ordenes() {
        $parametros = $this->input->post();
        $this->load->model('actualizar_orden_obrero_mod');
        $this->data['datos_tmp'] = $this->actualizar_orden_obrero_mod->seleccionar_orden($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }

    public function eliminar() {
        $parametros = $this->input->post();
        $this->load->model('actualizar_orden_obrero_mod');
        $this->data['datos_tmp'] = $this->actualizar_orden_obrero_mod->eliminar($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }

    public function actualizar() {
        $parametros = $this->input->post();
        $this->load->model('actualizar_orden_obrero_mod');
        $this->data['datos_tmp'] = $this->actualizar_orden_obrero_mod->actualizar($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }

    public function seleccionar_anio_dotacion() {
        $parametros = $this->input->post();
        $this->load->model('actualizar_orden_obrero_mod');
        $this->data['datos_tmp'] = $this->actualizar_orden_obrero_mod->seleccionar_anio_dotacion($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }

    public function actualiza_inventario() {
        $parametros = $this->input->post();
        $this->load->model('actualizar_orden_obrero_mod');
        $this->data['datos_tmp'] = $this->actualizar_orden_obrero_mod->inventario_actualizar($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }

    public function generar_reporte() {
        $parametros = $this->input->post();
        $this->load->model('actualizar_orden_obrero_mod');

        ////creamos el PDF
        $this->pdf = new Pdf();
        $this->pdf->AddPage();
        $this->pdf->AliasNbPages();
        $this->pdf->SetTitle('ORDEN OBRERO ENTREGADA');
        $this->pdf->SetFillColor(52, 152, 219);
        $this->pdf->SetFont('Arial', 'B', 12);
        $this->pdf->Cell(200, 10, 'ORDEN OBRERO ENTREGADA', 0, 0, 'C');
        $this->pdf->SetFont('Arial', 'B', 8);


        //////Titulos de las columnas//////////
        $this->pdf->SetXY(7, 50);
        $this->pdf->Cell(15, 10, 'CEDULA', 1, 0, 'C', 1);
        $this->pdf->Cell(25, 10, 'NOMBRE', 1, 0, 'C', 1);
        $this->pdf->Cell(20, 10, 'APELLIDO', 1, 0, 'C', 1);
        $this->pdf->Cell(50, 10, 'UNIDAD', 1, 0, 'C', 1);
        $this->pdf->Cell(50, 10, 'CARGO', 1, 0, 'C', 1);
        $this->pdf->Cell(20, 10, 'DOTACION', 1, 0, 'C', 1);
        $this->pdf->Cell(10, 10, utf8_decode('AÑO'), 1, 0, 'C', 1);
        $this->pdf->Ln(10);

        $datos = $this->actualizar_orden_obrero_mod->generar_reporte($parametros);

        foreach ($datos as $dato) {
            $this->pdf->SetX(7);
            $this->pdf->SetFont('Arial', '', 7  );
            $this->pdf->Cell(15, 5, $dato->cedula, 1, 0, 'C', 0);
            $this->pdf->Cell(25, 5, utf8_decode($dato->nombres), 1, 0, 'L', 0);
            $this->pdf->Cell(20, 5, utf8_decode($dato->apellidos), 1, 0, 'L', 0);
            $this->pdf->Cell(50, 5, utf8_decode($dato->unidad), 1, 0, 'L', 0);
            $this->pdf->Cell(50, 5, utf8_decode($dato->cargo), 1, 0, 'L', 0);
            $this->pdf->Cell(20, 5, utf8_decode($dato->dotacion), 1, 0, 'L', 0);
            $this->pdf->Cell(10, 5, $dato->anio, 1, 0, 'C', 0);
            $this->pdf->Ln();
        }

        $pdfString = $this->pdf->Output("Reporte.pdf", 'S');
        $pdfBase64 = base64_encode($pdfString);
        $this->data['datos_tmp'] = 'data:application/pdf;base64,' . $pdfBase64;
        $this->load->view('paginas/datos_tmp', $this->data);
    }

}
?>