<?php
 
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class proveedor_con extends CI_Controller {
    var $modulos = '';
    function __construct() {
        parent::__construct();
        $this->load->model('Utilidades_db_mod');
        $this->load->helper('url');
        $this->load->library('funciones');
        $this->load->helper('form', 'url');
        $this->load->helper('js_css');
        $this->cargar_config();
        $this->funciones->verifica_sesion($this->Utilidades_db_mod->ambiente[0], FRONTEND, $this->modulos);
    }

    private function cargar_config() {
        //Parametros comunes en todos los metodos
        $this->data['ambiente'] = $this->Utilidades_db_mod->ambiente[1];
        $this->data['titulo_pagina'] = $this->config->item('nombre_sistema');
        $this->data['contenido'] = 'login_view';
        $this->data['mensaje'] = '';
        $this->data['ruta_js'] = $this->config->item('ruta_js');
        $this->data['ruta_css'] = $this->config->item('ruta_css');
        $this->data['ruta_js_sist'] = $this->config->item('ruta_js_sist');
        $this->data['ruta_css_sist'] = $this->config->item('ruta_css_sist');
        $this->data['css_sist'] = array('contenido.css', 'top_view.css', 'principal_view.css');
        $this->data['js_sist'] = array('top_view.js', 'proveedor_view.js');
        $this->data['salir'] = $this->config->item('logoff');
        $this->data['nombre_usuario'] = '';
    }

    function index() {
        $this->data['mostrar_cintillo'] = true;
        $this->data['mostrar_menu'] = 'principal';
        $this->data['menu_activo'] = '';
        if ($this->data['mostrar_menu']) {
            $this->load->model('Top_mod');
            $json_menu = json_decode($this->Top_mod->traer_menu());
            $menu_depurado[$this->data['mostrar_menu']] = $this->funciones->configurar_menu($json_menu, $this->data['mostrar_menu']);
            $this->data['menu_todos'] = json_encode($menu_depurado);
        }
        $this->data['title'] = "Pagina Principal";
        $this->data['contenido'] = 'proveedor_view';
        $this->data['mensaje'] = '';
        $this->data['nombre_usuario'] = $_SESSION['login'];
        $this->load->view('plantillas/plantilla_view', $this->data);

    }

    public function __destruct() {
        $this->Utilidades_db_mod = null;
        $this->data = null;
    }
    public function eliminar() {
        $parametros = $this->input->post();
        $this->load->model('proveedor_mod');
        $this->data['datos_tmp'] = $this->proveedor_mod->eliminar_proveedor($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }

    public function actualizar_proveedor_existente() {
        $parametros = $this->input->post();
        $this->load->model('proveedor_mod');
        $this->data['datos_tmp'] = $this->proveedor_mod->actualizar_proveedor_existente($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }
    
    public function guardar_dotacion_proveedor() {
        $parametros = $this->input->post();
        $this->load->model('proveedor_mod');
        $this->data['datos_tmp'] = $this->proveedor_mod->guardar_dotacion_proveedor($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }

    public function tabla_proveedor() {
        $parametros = $this->input->post();
        $this->load->model('proveedor_mod');
        $this->data['datos_tmp'] = $this->proveedor_mod->tabla_proveedor($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }

    public function seleccionar_orden() {
        $parametros = $this->input->post();
        $this->load->model('proveedor_mod');
        $this->data['datos_tmp'] = $this->proveedor_mod->seleccionar_orden($parametros);
        $this->load->view('paginas/datos_tmp', $this->data);
    }
}
?>