$(document).on('ready', function(){
	var str_url = window.location.href;
	var anio;
	var selectVal;
	seleccionar_anio();
	seleccionar_dotacion();
	seleccionar_tipo_rubro();
	seleccionar_talla();
	seleccionar_anio_dotacion();

    $("#div_mostrar").css("display", "none");

    function seleccionar_anio() {
		var str_url_bc = str_url + '/seleccionar_anio/';
		$.post(str_url_bc, function (resultado) {
			$.each(resultado.registros, function (i, item) {
				$('#anio').append('<option >' + item.anio + '</option>');
			});
		}, 'json')

		.fail(function () {
			alert("error año");
		});
	}

	function seleccionar_anio_dotacion() {
		var str_url_bc = str_url + '/seleccionar_anio_dotacion/';
		$.post(str_url_bc, function (resultado) {
			$.each(resultado.registros, function (i, item) {
				$('#aniodotacion').append('<option >' + item.aniodotacion + '</option>');
			});
		}, 'json')

		.fail(function () {
			alert("error año dotacion");
		});
	}

    function seleccionar_dotacion() {
        var str_url_bc = str_url + '/seleccionar_dotacion/';
        $.post(str_url_bc, function (resultado) {
            $.each(resultado.registros, function (i, item) {
                $('#dotacion').append('<option value='+item.id+'>' + item.descripcion + '</option>');
            });
        }, 'json')

                .fail(function () {
                    alert("error dotacion");
                });
    }

    function seleccionar_tipo_rubro() {
        var str_url_bc = str_url + '/seleccionar_tiporubro/';
        $.post(str_url_bc, function (resultado) {
            $.each(resultado.registros, function (i, item) {
                $('.tipo_rubro').append('<option value='+item.id+' >' + item.descripcion + '</option>');
            });
        }, 'json')

                .fail(function () {
                    alert("error tipo de rubro");
                });
    }

    function seleccionar_rubro(selectVal, fila) {
        var str_url_bc = str_url + '/seleccionar_rubro/';
        $.post(str_url_bc, {'id_tipo_rubro': selectVal}, function (resultado) {
            if (resultado.registros == '') {
                alert('No existe rubro asociado a este tipo de rubro!');
            } else {
                $.each(resultado.registros, function (i, item) {
                    $('#rubro_'+fila).append('<option value=' + item.id + '>' + item.rubro + '</option>');
                })
            }
            ;
        }, 'json')

                .fail(function () {
                    alert("error en el rubro");
                });
    }
    function seleccionar_talla() {
        var str_url_bc = str_url + '/seleccionar_tallas/';
        $.post(str_url_bc, function (resultado) {
            $.each(resultado.registros, function (i, item) {
                $('.talla').append('<option value='+item.id+' >' + item.talla + '</option>');
            });
        }, 'json')

                .fail(function () {
                    alert("error talla");
                });
    }

    $('#aniodotacion').on('change', function(){
    	selectVal= $('#aniodotacion option:selected').val();
    });

    $('.talla').on('change', function(){
        selectVal = $(".talla option:selected").val();

    });

   $('.tipo_rubro').on('change', function(){
        var res = $(this).attr('id').split("_");
        selectVal= $(this).val();
        $('#rubro_'+res[1]).empty();
        $('#cantidad_'+res[1]).empty();
        seleccionar_rubro(selectVal,res[1]);
    });

   $('#fecha_a').datetimepicker({
        locale: 'es',
        format: 'D/M/YYYY'
    });

    $('#fecha_b').datetimepicker({
        locale: 'es',
        format: 'D/M/YYYY'
    });

    $('#dotacion').on('change', function () {
        selectVal = $("#dotacion option:selected").val();
    });

	$('#anio').on('change', function(){
		anio= $('#anio option:selected').val();
        tabla_listar_empleados();
        tabla_empleado.dataTable().fnClearTable();
        tabla_empleado.dataTable().fnDestroy();
	});

    $('#actualizar').on('click', function(){
        actualizar();
        inventario_actualizar();    

    });

    $('#eliminar').on('click', function(){
    	if(aviso_eliminar()){
    		eliminar_rubro();
            limpiar();
    	}
    });
    $('#btn_generar_reporte').on('click', function(){
        generar_reporte();
    });
    

    function limpiar() {
        $(".tipo_rubro").val("Tipo de rubro");
        $(".rubro").val("Rubro");
        $(".cantidad").val("");
        $(".talla").val("Talla");
    }

	function tabla_listar_empleados() {
        tabla_empleado = $("#tabla_empleado");
        var str_url_bc = str_url + '/tabla_listar_empleados';
        anio= $('#anio').val();
        $("#tabla_empleado").DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "Ningun elemento encontrado",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrado de _MAX_ registros totales)",
                "search": "Búsqueda:",
                "info": "Mostrando _START_ - _END_ de _TOTAL_ registros",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "paginate": {
                    "first": "Primera",
                    "last": "Última",
                    "next": "Sig",
                    "previous": "Ant"
                }
            },

            "ajax": {
                "url": str_url_bc,
                "dataSrc": "registros",
                "dataType": "json",
                "type": "POST",
                "data": {"t_anio": anio}
            },
 
            "columns": [
                {"data": "cedula"},
                {"data": "nombres"},
                {"data": "apellidos"},
                {"data": "unidad"},
                {"data": "cargo"},
                {"data": "dotacion"},
                {"data": "anio"},
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": "<button class='glyphicon glyphicon-eye-open btn btn-primary'></button>"
                }
            ],

            "initComplete": function (settings, resultado) {
                if (resultado.registros == '') {
                    $("#div_mostrar").css("display", "none");
                    alert('No existe dotacion registrada!!');
                } else {
                    $("#div_mostrar").show();
                    $.busqueda = resultado;
                    $("td").click(function () {
                        var oID = $(this).attr("cedula");
                        $.traer_registro(oID, $.busqueda.registros);
                        $("#myModal").modal();
                    });
                }
            },

            "createdRow": function (row, data, index) {
                $('td', row).eq(7).attr('cedula', data.cedula);
                $('td', row).eq(7).attr('style', 'cursor: pointer');
                $('td', row).eq(7).attr('title', 'Ver detalle de orden.');
            },
        });
    }

    $.traer_registro = function (id, items) {
        var oID = id;
        $.each(items, function (indice, reg) {
            if (oID === reg.cedula) {
                oID = 0;
                $("#title_por_entregar").text(reg.nombres + reg.apellidos + ' - ' + reg.cedula + ' - ' + reg.cargo);
                $.cedula = reg.cedula;
                seleccionar_ordenes(reg.cedula);
            }
        });
    };

	$(".añadir").each(function(el) {
        $(this).bind("click", addField);
    });

    function addField() {
    // ID del elemento div quitandole la palabra "div_" de delante. Pasi asi poder aumentar el número. Esta parte no es necesaria pero yo la utilizaba ya que cada campo de mi formulario tenia un autosuggest , así que dejo como seria por si a alguien le hace falta. 
            var clickID = parseInt($(this).parent('div').parent('div').attr('id').replace('div_', ''));

    // Genero el nuevo numero id
            var newID = (clickID + 1);

    // Creo un clon del elemento div que contiene los campos de texto
            var $newClone = $('#div_' + clickID).clone(true);

    //Le asigno el nuevo numero id
            $newClone.attr("id", 'div_' + newID);

    //Asigno nuevo id al primer campo input dentro del div y le borro cualquier valor que tenga asi no copia lo ultimo que hayas escrito.(igual que antes no es necesario tener un id)
            $newClone.children("div").children("select").eq(0).attr("id", 'tiporubro_' + newID).val('Tipo de rubro');
            $newClone.children("div").children("select").eq(1).attr("id", 'rubro_' + newID).val('Rubro');
            $newClone.children("div").children("select").eq(2).attr("id", 'talla_' + newID).val('Talla');
            $newClone.children("div").children("input").eq(0).attr("id", 'cantidad_' + newID).val('');
    //Asigno nuevo id al boton
            $newClone.children("div").children("input").eq(1).attr("id", newID)

    //Inserto el div clonado y modificado despues del div original
            $newClone.insertAfter($('#div_' + clickID));

    //Cambio el signo "+" por el signo "-" y le quito el evento addfield
            $("#" + clickID).val('-').unbind("click", addField);

    //Ahora le asigno el evento delRow para que borre la fial en caso de hacer click
            $("#" + clickID).bind("click", delRow);

        }

    function delRow() {
    // Funcion que destruye el elemento actual una vez echo el click
        $(this).parent('div').parent('div').remove();
    }
    function limpiar_campos() {
        $(".tipo_rubro").val("Tipo de rubro");
        $(".rubro").val("Rubro");
        $(".cantidad").val("");
        $(".talla").val("Talla");
        $("#fecha_a").val("");
        $("#fecha_b").val("");
        $("#dotacion").val("Seleccione Dotacion");
        $("#anio").val("Seleccione año");
    }

    function seleccionar_ordenes(id_cedula) {
        var str_url_bc = str_url + '/seleccionar_orden/';
        //limpiar_campos();
        $('#cedula').val(id_cedula);
        $.post(str_url_bc, {'cedula': id_cedula},
        function (resultado) {
        $.each(resultado.registros, function (i, item) {
            if (id_cedula === item.cedula) {
                //$(".rubro").val(item.rubro);
                $(".cantidad").val(item.cantidad);
                $(".rubro option:selected").text();
                $(".tipo_rubro option:selected").text();
                $(".rubro").val(item.rubro);
                $(".talla").val(item.talla);
                $("#fecha_a").val(item.fecha1);
                $("#fecha_b").val(item.fecha2);
                $("#dotacion").val(item.dotacion);
                $('#anio').val(item.anio);

            }
        });
        }, 'json')

                .fail(function () {
                    alert("error al seleccionar orden");
                });
    }


    function aviso_eliminar() {
        if (!confirm("¿Desea eliminar este rubro? \n"))
            return false;
        else
            return true;
    }

    function eliminar_rubro(){
        var str_url_bc = str_url + '/eliminar_rubro/';
        $.post(str_url_bc,{'cedula':$('#cedula').val()},
        function(resultado) {
            if (resultado.mensaje == 'eliminado') {
                alert('El rubro se ha aliminado con exito');
            } 
            else{
                alert('Error al eliminar rubro');
            }
        },
        'json'
        )
        .fail(function() {
            alert("No elimino");
        });
    }

    function inventario_actualizar() {
        var str_url_bc = str_url + '/actualizar_inventario/';
        var rubro= [];
        var cantidad= [];
        var talla= [];
        var sw=0;
        $('select[name="rubro[]"]').each(function() {
            if ($(this).val() != '') {
                rubro[sw] = $(this).val();
                sw++;

            }
        });
        sw=0;
        $('input[name="cantidad[]"]').each(function() {
            if ($(this).val() != '') {
                cantidad[sw] = $(this).val();
                sw++;

            }
        });
        sw=0;
        $('select[name="talla[]"]').each(function() {
            if ($(this).val() != '') {
                talla[sw] = $(this).val();
                sw++;

            }
        });
        $.post(str_url_bc, {
            'id_entrega': $("#id_entrega").val(),
            'rubro': rubro,
            'cantidad': cantidad,
            'talla': talla,
            'cedula': $("#cedula").val()
        },
        function (resultado) {
            if (resultado.mensaje == 'actualiza') {
                alert('Inventario actualizado');
                tabla_obrero.dataTable().fnClearTable();
                tabla_obrero.dataTable().fnDestroy();
                tabla_listar_obreros();
                $("#myModal").modal('hide');
            }
        }, 'json')

        .fail(function () {
            alert("No actualizo Inventario");
        });
    }

    function actualizar() {
        var str_url_bc = str_url + '/actualizar_orden/';
        var rubro= [];
        var cantidad= [];
        var talla= [];
        var sw=0;
        $('select[name="rubro[]"]').each(function() {
            if ($(this).val() != '') {
                rubro[sw] = $(this).val();
                sw++;

            }
        });
        sw=0;
        $('input[name="cantidad[]"]').each(function() {
            if ($(this).val() != '') {
                cantidad[sw] = $(this).val();
                sw++;

            }
        });
        sw=0;
        $('select[name="talla[]"]').each(function() {
            if ($(this).val() != '') {
                talla[sw] = $(this).val();
                sw++;

            }
        });
        $.post(str_url_bc, {
            'id_entrega': $("#id_entrega").val(),
            'rubro': rubro,
            'cantidad': cantidad,
            'talla': talla,
            'fecha1': $("#fecha_a").val(),
            'fecha2': $("#fecha_b").val(),
            'cedula': $("#cedula").val(),
            'dotacion': $("#dotacion").val(),
            'anio': $("#anio").val()
        },
        function (resultado) {
            if (resultado.mensaje == 'actualizado') {
                alert('Actualizado con exito');
                tabla_empleado.dataTable().fnClearTable();
                tabla_empleado.dataTable().fnDestroy();
                tabla_listar_empleados();
                $("#myModal").modal('hide');
            }
        }, 'json')

        .fail(function () {
            alert("No actualizo");
        });
    }

    function generar_reporte() {
        var str_url_bc = str_url + '/imprimir_reporte/';
        var respuesta_ajax = $.ajax({
            url: str_url_bc,
            data: "tabla=" + "tabla",
            cache: false,
            processData: false,
            type: 'POST',
            method: "post",
            error: function () {
                var msj = crear_mensaje("Error", "danger");
                ver_mensaje(msj);
            }
        }).always(function () {

        });
        $.when(respuesta_ajax).done(function (ajaxValue) {
            var win = window.open('', '_blank');
            win.location.href = ajaxValue;
            var msj = crear_mensaje("Proceso Finalizado", "info");
            ver_mensaje(msj);
        });

    }




});