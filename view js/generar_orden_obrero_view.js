$.busqueda = new Object();
$.cedula = 0;

$(document).on('ready', function() {
  var str_url = window.location.href;  
  var selectValClausula;
  var selectValunidad;
  var selectVal;
  var selectanio;
  var t_nomina = 10;
  clausulas_con_relaciones();
  seleccionar_anio();
  seleccionar_dotacion();
  seleccionar_tipo_rubro();
  seleccionar_talla();
  $("#div_mostrar").css("display", "none");
  
        function clausulas_con_relaciones() {
        var str_url_bc = str_url + '/clausulas_con_relaciones/';
        $.post(
                str_url_bc,
                function (resultado) {
                    var fecha = new Date();
                    var anio = fecha.getFullYear();
                    $.each(resultado.registros, function (i, item) {
                        var opcion = $('<option/>').text(item.aniodotacion);
                        $('#clausula').append(opcion);
                        if (anio.toString() === opcion.text()) {
                            $('#clausula').val(opcion.text()).trigger('change');
                        }
                        seleccionar_unidad($('#clausula').val());

                    });
                },
                'json'
                )
                .fail(function () {
                    alert("error con el año de dotacion");
                });
    }
 
    function seleccionar_unidad(anio) {
        var a = anio;
        var str_url_bc = str_url + '/seleccionar_unidades/';
        $('#unidad').empty();
        $('#unidad').append($('<option/>').text('Seleccione una opcion'));
        $.post(
                str_url_bc,
                {
                    anno: a,
                    t_nomina: t_nomina
                },
                function (resultado) {
                    $.each(resultado.registros, function (i, item) {

                        $('#unidad').append('<option >' + item.unidad + '</option>');
                    });
                    $('#unidad').trigger('change');
                },
                'json')

                .fail(function () {
                    alert("error al seleccionar la unidad");
                });
    }
    function seleccionar_anio(){
      var str_url_bc = str_url + '/seleccionar_anios/';
      $.post(str_url_bc,function(resultado) {
        $.each(resultado.registros, function (i, item) {
          $('#anio').append('<option >'+item.aniodotacion+'</option>');
      });
      },'json')
    
      .fail(function() {
        alert("error año");
      });
    }

    function seleccionar_dotacion() {
        var str_url_bc = str_url + '/seleccionar_dotacion/';
        $.post(str_url_bc, function (resultado) {
            $.each(resultado.registros, function (i, item) {
                $('#dotacion').append('<option value='+item.id+'>' + item.descripcion + '</option>');
            });
        }, 'json')

                .fail(function () {
                    alert("error dotacion");
                });
    }
    function seleccionar_tipo_rubro() {
        var str_url_bc = str_url + '/seleccionar_tiporubro/';
        $.post(str_url_bc, function (resultado) {
            $.each(resultado.registros, function (i, item) {
                $('.tipo_rubro').append('<option value='+item.id+' >' + item.descripcion + '</option>');
            });
        }, 'json')

                .fail(function () {
                    alert("error tipo de rubro");
                });
    }

    function seleccionar_rubro(selectVal, fila) {
        var str_url_bc = str_url + '/seleccionar_rubro/';
        $.post(str_url_bc, {'id_tipo_rubro': selectVal}, function (resultado) {
            if (resultado.registros == '') {
                alert('No existe rubro asociado a este tipo de rubro!');
            } else {
                $.each(resultado.registros, function (i, item) {
                    $('#rubro_'+fila).append('<option value=' + item.id + '>' + item.rubro + '</option>');
                })
            }
            ;
        }, 'json')

                .fail(function () {
                    alert("error en el rubro");
                });
    }
    function seleccionar_talla() {
        var str_url_bc = str_url + '/seleccionar_tallas/';
        $.post(str_url_bc, function (resultado) {
            $.each(resultado.registros, function (i, item) {
                $('.talla').append('<option value='+item.id+' >' + item.talla + '</option>');
            });
        }, 'json')

                .fail(function () {
                    alert("error talla");
                });
    }

    $('.talla').on('change', function(){
        selectVal = $(".talla option:selected").val();

    });

   $('.tipo_rubro').on('change', function(){
        var res = $(this).attr('id').split("_");
        selectVal= $(this).val();
        $('#rubro_'+res[1]).empty();
        $('#cantidad_'+res[1]).empty();
        seleccionar_rubro(selectVal,res[1]);
    });

    $('#clausula').on('change', function (event) {
        var anio = $(this).val();
        selectValClausula = $("#clausula option:selected").val();
        seleccionar_unidad(anio);
    });

  $('#btn_generar_reporte').click(function () {
      generar_reporte();
  });

  $('#anio').change(function(){
      selectanio = $("#anio option:selected").val();
  }); 

  $('#unidad').on('change', function(){
    selectValunidad = $("#unidad option:selected").val();
  });  
 
  $('#btn_seleccionar').click(function(){
     tabla_listar_obreros();
     tabla_obrero.dataTable().fnClearTable();
     tabla_obrero.dataTable().fnDestroy();
  });               
 
  $('#fecha_a').datetimepicker({
    locale: 'es',
    format:'D/M/YYYY'
  });

  $('#fecha_b').datetimepicker({
    locale: 'es',
    format:'D/M/YYYY'
  });

  $('#dotacion').on('change', function(){
    selectVal = $("#dotacion option:selected").val();
  });

   

  function tabla_listar_obreros(){
    tabla_obrero= $("#tabla_obrero");
    var str_url_bc = str_url + '/tabla_listar_obreros';
    if(!selectValunidad){
            selectValunidad='Seleccione una opcion';
        }
        selectValClausula= $('#clausula').val();
    $("#tabla_obrero").DataTable( {
      "language": {
        "lengthMenu": "Mostrar _MENU_ registros",
        "zeroRecords": "Ningun elemento encontrado",
        "infoEmpty": "No hay registros disponibles",
        "infoFiltered": "(filtrado de _MAX_ registros totales)",
        "search": "Búsqueda:",
        "info":           "Mostrando _START_ - _END_ de _TOTAL_ registros",
        "loadingRecords": "Cargando...",
        "processing":     "Procesando...",
        "paginate": {
          "first":      "Primera",
          "last":       "Última",
          "next":       "Sig",
          "previous":   "Ant"
        }
      },
         
      "ajax":{
        "url":str_url_bc,
        "dataSrc":"registros",
        "dataType":"json",
        "type": "POST",
        "data":{"t_nomina": t_nomina, "t_anio": selectValClausula, "t_unidad": selectValunidad}
      },

      "columns": [
      	{ "data": "ubicacion" },
      	{ "data": "unidad" },
      	{ "data": "nombres" },
      	{ "data": "apellidos" },
        { "data": "cedula","orderable":      true },
        { "data": "fechaing" },
        { "data": "cargo" },
        { "data": "dotacion" },
        { "data": "sexo" },
        { "data": "nomina" },
        { "data": "calzado" },
        { "data": "pantalon" },
        { "data": "camisa" },
        { "data": "chaqueta" },
        {
          "className":      'details-control',
          "orderable":      false,
          "data":           null,
          "defaultContent": "<button class='glyphicon glyphicon-eye-open btn btn-primary'></button>"
        } 
      ],


      "initComplete": function(settings,resultado){
        if (resultado.registros == '') {
          $("#div_mostrar").css("display", "none");
          alert('No existe dotacion registrada!!');
        }
        else {
          $("#div_mostrar").show();
          $.busqueda = resultado;
          $("td").click(function() {
            var oID = $(this).attr("cedula");                    
            $.traer_registro(oID, $.busqueda.registros);
            $("#myModal").modal();
          });
        }
      },

      "createdRow": function ( row, data, index ) {
        
        $('td', row).eq(14).attr('cedula',data.cedula);
        $('td', row).eq(14).attr('style','cursor: pointer');
        $('td', row).eq(14).attr('title','Ver detalle de orden.');
      },
      
    });
  }

  $.traer_registro = function(id, items){
    var oID = id;
    $.each(items, function(indice, reg){
      if (oID === reg.cedula) {
        oID = 0;
        $("#title_por_entregar").text(reg.nombres + reg.apellidos + ' - ' + reg.cedula + ' - ' + reg.cargo);
        $.cedula = reg.cedula;
        seleccionar_ordenes(reg.cedula);
      }
    });
  };

    $(".añadir").each(function(el) {
        $(this).bind("click", addField);
    });

    function addField() {
    // ID del elemento div quitandole la palabra "div_" de delante. Pasi asi poder aumentar el número. Esta parte no es necesaria pero yo la utilizaba ya que cada campo de mi formulario tenia un autosuggest , así que dejo como seria por si a alguien le hace falta. 
            var clickID = parseInt($(this).parent('div').parent('div').attr('id').replace('div_', ''));

    // Genero el nuevo numero id
            var newID = (clickID + 1);

    // Creo un clon del elemento div que contiene los campos de texto
            var $newClone = $('#div_' + clickID).clone(true);

    //Le asigno el nuevo numero id
            $newClone.attr("id", 'div_' + newID);

    //Asigno nuevo id al primer campo input dentro del div y le borro cualquier valor que tenga asi no copia lo ultimo que hayas escrito.(igual que antes no es necesario tener un id)
            $newClone.children("div").children("select").eq(0).attr("id", 'tiporubro_' + newID).val('Tipo de rubro');
            $newClone.children("div").children("select").eq(1).attr("id", 'rubro_' + newID).val('Rubro');
            $newClone.children("div").children("input").eq(0).attr("id", 'cantidad_' + newID).val('');
    //Asigno nuevo id al boton
            $newClone.children("div").children("input").eq(1).attr("id", newID)

    //Inserto el div clonado y modificado despues del div original
            $newClone.insertAfter($('#div_' + clickID));

    //Cambio el signo "+" por el signo "-" y le quito el evento addfield
            $("#" + clickID).val('-').unbind("click", addField);

    //Ahora le asigno el evento delRow para que borre la fial en caso de hacer click
            $("#" + clickID).bind("click", delRow);

        }

    function delRow() {
    // Funcion que destruye el elemento actual una vez echo el click
        $(this).parent('div').parent('div').remove();
        if(aviso_eliminar()){
            eliminar_rubro();
        }
    }

    $("#guardar_orden_entrega").click(function (event) {
        event.preventDefault(); 
        if (campos_vacios()) {
            if (aviso()) {
                insertar_dotacion_cargo_obrero();
                inventario_actualizar();
            }
        }
        
    });


    function campos_vacios() {
        var varAll = false;
        if (!$('.tipo_rubro').val().trim()) {
            alert('Debe especificar el tipo de rubro');
            $('.tipo_rubro').focus();
            return false;
        }

        if ($('.rubro').val() === 'Seleccione Rubro') {
            alert('Debe seleccionar el rubro');
            $('.rubro').focus();
            return false;
        }
        if (!$('.cantidad').val()=== '') {
            alert('Debe especificar la cantidad');
            $('.cantidad').focus();
            return false;
        }
        return true;
    }

    function aviso() {
        if (!confirm("¿Desea guardar esta dotacion? \n"))
            return false;
        else
            return true;
    }
    function limpiar_campos() {
        $(".tipo_rubro").val("Tipo de rubro");
        $(".rubro").val("Rubro");
        $(".cantidad").val("");
        $(".talla").val("Talla");
        $("#fecha_a").val("");
        $("#fecha_b").val("");
        $("#dotacion").val("Seleccione Dotacion");
        $("#anio").val("Seleccione año");
    }
  function seleccionar_ordenes(id_cedula) {
        var str_url_bc = str_url + '/seleccionar_ordenes/';
        limpiar_campos();
        $('#cedula').val(id_cedula);
        $.post(str_url_bc, {'cedula': id_cedula}, function (resultado) {
            $.each(resultado.registros, function (i, item) {
                if (id_cedula === item.cedula) {
                    //$(".rubro").val(item.rubro);
                    $(".cantidad").val(item.cantidad);
                    $(".rubro option:selected").stext();
                    $(".tipo_rubro option:selected").text();
                    $(".talla option:selected").text();
                    $(".rubro").val(item.rubro);
                    $(".talla").val(item.talla);
                    $("#fecha_a").val(item.fecha1);
                    $("#fecha_b").val(item.fecha2);
                    $("#dotacion").val(item.dotacion);
                    $('#anio').val(item.anio);

                }
            });
        }, 'json')

                .fail(function () {
                    alert("error al seleccionar orden");
                });
    }

    function inventario_actualizar() {
        var str_url_bc = str_url + '/actualizar_inventario/';
        var rubro= [];
        var cantidad= [];
        var talla= [];
        var sw=0;
        $('select[name="rubro[]"]').each(function() {
            if ($(this).val() != '') {
                rubro[sw] = $(this).val();
                sw++;

            }
        });
        sw=0;
        $('input[name="cantidad[]"]').each(function() {
            if ($(this).val() != '') {
                cantidad[sw] = $(this).val();
                sw++;

            }
        });
        sw=0;
        $('select[name="talla[]"]').each(function() {
            if ($(this).val() != '') {
                talla[sw] = $(this).val();
                sw++;

            }
        });
        $.post(str_url_bc, {
            'id_entrega': $("#id_entrega").val(),
            'rubro': rubro,
            'cantidad': cantidad,
            'talla': talla,
            'cedula': $("#cedula").val()
        },
        function (resultado) {
            if (resultado.mensaje == 'actualiza') {
                alert('Inventario actualizado');
                tabla_obrero.dataTable().fnClearTable();
                tabla_obrero.dataTable().fnDestroy();
                tabla_listar_obreros();
                $("#myModal").modal('hide');
            }
        }, 'json')

        .fail(function () {
            alert("No actualizo Inventario");
        });
    }



    function insertar_dotacion_cargo_obrero() {
        var str_url_bc = str_url + '/guardar_dotacion_cargo_obrero/';
        var rubro= [];
        var cantidad= [];
        var talla= [];
        var sw=0;
        $('select[name="rubro[]"]').each(function() {
            if ($(this).val() != '') {
                rubro[sw] = $(this).val();
                sw++;

            }
        });
        sw=0;
        $('input[name="cantidad[]"]').each(function() {
            if ($(this).val() != '') {
                cantidad[sw] = $(this).val();
                sw++;

            }
        });
        sw=0;
        $('select[name="talla[]"]').each(function() {
            if ($(this).val() != '') {
                talla[sw] = $(this).val();
                sw++;

            }
        });
        $.post(str_url_bc, {
            'id_entrega': $("#id_entrega").val(),
            'rubro': rubro,
            'cantidad': cantidad,
            'talla': talla,
            'fecha1': $("#fecha_a").val(),
            'fecha2': $("#fecha_b").val(),
            'cedula': $("#cedula").val(),
            'dotacion': $("#dotacion").val(),
            'anio': $("#anio").val()
        },
        function (resultado) {
            if (resultado.mensaje == 'agregado') {
                alert('Guardado con exito');
                tabla_obrero.dataTable().fnClearTable();
                tabla_obrero.dataTable().fnDestroy();
                tabla_listar_obreros();
                $("#myModal").modal('hide');
            }
            if (resultado.mensaje == 'existe') {
                alert('esta relacion ya existe');
            }
            if (resultado.mensaje == 'actualizado') {
                //alert('Relacion modificada exitosamente.');
                ver_mensaje(resultado.mensaje);
                tabla_obrero.dataTable().fnClearTable();
                tabla_obrero.dataTable().fnDestroy();
                tabla_listar_obreros();
                $("#myModal").modal('hide');
            }
        }, 'json')

        .fail(function () {
            alert("No guardo");
        });
    }

    function actualiza_rubro() {
        var str_url_bc = str_url + '/actualiza_rubro/';
        var rubro= [];
        var cantidad= [];
        var talla= [];
        var sw=0;
        $('select[name="rubro[]"]').each(function() {
            if ($(this).val() != '') {
                rubro[sw] = $(this).val();
                sw++;

            }
        });
        sw=0;
        $('input[name="cantidad[]"]').each(function() {
            if ($(this).val() != '') {
                cantidad[sw] = $(this).val();
                sw++;

            }
        });
        sw=0;
        $('select[name="talla[]"]').each(function() {
            if ($(this).val() != '') {
                talla[sw] = $(this).val();
                sw++;

            }
        });
        $.post(str_url_bc, {
            'rubro': rubro,
            'cantidad': cantidad,
            'talla': talla,
            'cedula': $("#cedula").val()
        },
        function (resultado) {
            if (resultado.mensaje == 'actualizados') {
                alert('Relacion modificada exitosamente.');
                ver_mensaje(resultado.mensaje);
                tabla_obrero.dataTable().fnClearTable();
                tabla_obrero.dataTable().fnDestroy();
                tabla_listar_obreros();
                //$("#myModal").modal('hide');
            }
        }, 'json')

        .fail(function () {
            alert("No actualizo rubros");
        });
    }

    function actualiza_encabezado() {
        var str_url_bc = str_url + '/actualiza_encabezado/';
        $.post(str_url_bc, {
            'fecha1': $("#fecha_a").val(),
            'fecha2': $("#fecha_b").val(),
            'cedula': $("#cedula").val(),
            'dotacion': $("#dotacion").val(),
            'anio': $("#anio").val()
        },
        function (resultado) {
            if (resultado.mensaje == 'actualizado') {
                alert('Relacion modificada exitosamente.');
                ver_mensaje(resultado.mensaje);
                tabla_obrero.dataTable().fnClearTable();
                tabla_obrero.dataTable().fnDestroy();
                tabla_listar_obreros();
                $("#myModal").modal('hide');
            }
        }, 'json')

        .fail(function () {
            alert("No actualizo encabezado");
        });
    }

});