$(document).on('ready', function() {
    var str_url = window.location.href;
    var selectVal;
    seleccionar_proveedor();
    seleccionar_rubros();


// evento para que una ves posicionado en el focus lo seleccione
    $('input[type=text]').focus(function(){
        $(this).select();   
    });

   /* $("#añadir").click(function(){
        var clonarfila= $("#tbl-detalle").find("tbody tr:last").clone();
        clonarfila.find('input').val('');
        $("table tbody").append(clonarfila);
    });

    

    $("#tbl-detalle").on('click', '#eliminar', function(){
        var numeroFilas = $("#tbl-detalle tr").length;
        if(numeroFilas>2){
            $(this).closest('tr').remove();
        }
    });


    $('#tbl-detalle #precio').keyup(function(){
        var i = $('#precio').index(this);
        alert($(this).attr('id'));
        var cantidad= $('#cantidad:eq('+i+')').val();
        var total =(parseFloat(cantidad) * parseFloat($(this).val()));
        $('#total:eq('+i+')').val(total);
        //alert(i);
    });
*/

    $('#fecha').datetimepicker({
        locale: 'es',
        format: 'D/M/YYYY'
    });

    $(".añadir").each(function(el) {
        $(this).bind("click", addField);
    });

// clase para que una vez que salga del focus multiplique la cantidad por el precio para obtener el total
    $('.ver').focusout(function(){
        var res = $(this).attr('id').split("_");
        var resul=parseFloat($('#cantidad_'+res[1]).val())* parseFloat($('#precio_'+res[1]).val());
        $('#total_' +res[1]).val(resul); 

    });


    function addField() {
    // ID del elemento div quitandole la palabra "div_" de delante. Pasi asi poder aumentar el número. Esta parte no es necesaria pero yo la utilizaba ya que cada campo de mi formulario tenia un autosuggest , así que dejo como seria por si a alguien le hace falta. 
            var clickID = parseInt($(this).parent('div').parent('div').attr('id').replace('div_', ''));

    // Genero el nuevo numero id
            var newID = (clickID + 1);

    // Creo un clon del elemento div que contiene los campos de texto
            var $newClone = $('#div_' + clickID).clone(true);

    //Le asigno el nuevo numero id
            $newClone.attr("id", 'div_' + newID);

    //Asigno nuevo id al primer campo input dentro del div y le borro cualquier valor que tenga asi no copia lo ultimo que hayas escrito.(igual que antes no es necesario tener un id)
            $newClone.children("div").children("select").eq(0).attr("id", 'rubro_' + newID).val('Tipo de rubro');
            $newClone.children("div").children("input").eq(0).attr("id", 'cantidad_' + newID).val('');
            $newClone.children("div").children("input").eq(1).attr("id", 'precio_' + newID).val('');
            $newClone.children("div").children("input").eq(2).attr("id", 'total_' + newID).val('');
    //Asigno nuevo id al boton
            $newClone.children("div").children("input").eq(3).attr("id", newID)

    //Inserto el div clonado y modificado despues del div original
            $newClone.insertAfter($('#div_' + clickID));

    //Cambio el signo "+" por el signo "-" y le quito el evento addfield
            $("#" + clickID).val('-').unbind("click", addField);

    //Ahora le asigno el evento delRow para que borre la fial en caso de hacer click
            $("#" + clickID).bind("click", delRow);

        }

    function delRow() {
    // Funcion que destruye el elemento actual una vez echo el click
      $(this).parent('div').parent('div').remove();
    }


//funcion para guardar la factura segun todo lo ingresado
    function guardar_factura() {
        var str_url_bc = str_url + '/guardar_facturas/';
        var rubro= [];
        var cantidad= [];
        var precio= [];
        var total= [];
        var sw=0;
        $('select[name="rubro[]"]').each(function() {
            if ($(this).val() != '') {
                rubro[sw] = $(this).val();
                sw++;

            }
        });
        sw=0;
        $('input[name="cantidad[]"]').each(function() {
            if ($(this).val() != '') {
                cantidad[sw] = $(this).val();
                sw++;

            }
        });
        sw=0;
        $('input[name="precio[]"]').each(function() {
            if ($(this).val() != '') {
                precio[sw] = $(this).val();
                sw++;

            }
        });
        sw=0;
        $('input[name="total[]"]').each(function() {
            if ($(this).val() != '') {
                total[sw] = $(this).val();
                sw++;

            }
        });
        $.post(str_url_bc, {
            'id_factura': $("#id_factura").val(),
            'tipo_rubro': rubro,
            'cantidad': cantidad,
            'preciounitario': precio,
            'total': total,
            'num_factura': $("#numero_factura").val(),
            'fecha_recibido': $("#fecha").val(),
            'id_proveedor': $("#proveedor").val()
        },
                function (resultado) {
                    if (resultado.mensaje == 'agregado') {
                        alert('Guardado con Exito')
                        limpiar_campos();
                    }
                    if (resultado.mensaje == 'existe') {
                        alert('Esta relacion ya existe');
                    }
                }, 'json')

                .fail(function () {
                    alert("No guardo");
                });
    }

   function limpiar_campos() {
        $("#numero_factura").val("");
        $("#proveedor").val("Seleccione Proveedor");
        $("#fecha").val("");
        $(".rubro").val("Tipo de rubro");
        $(".ver").val("");
        $(".total").val("");
    }

    $('#guardar').on('click', function () {
        guardar_factura();
    });

//funcion para llenar el combobox del proveedor

    function seleccionar_proveedor() {
        var str_url_bc = str_url + '/seleccionar_proveedor/';
        $.post(str_url_bc, function (resultado) {
            $.each(resultado.registros, function (i, item) {
                $('#proveedor').append('<option >' + item.rif + '</option>');
            });
        }, 'json')

                .fail(function () {
                    alert("error proveedor");
                });
    }
//evento para seleccionar el  proveedor

    $('#proveedor').on('change', function () {
        selectVal = $("#proveedor option:selected").val();
    });
//funcion para llenar el combobox de los rubros
    function seleccionar_rubros() {
        var str_url_bc = str_url + '/seleccionar_rubro/';
        $.post(str_url_bc, function (resultado) {
            $.each(resultado.registros, function (i, item) {
                $('.rubro').append('<option value=' + item.id + ' >' + item.rubro + '</option>');
            });
        }, 'json')

                .fail(function () {
                    alert("error rubro");
                });
    }
//evento para seleccionar el rubro
    $('.rubro').on('change', function () {
        selectVal = $(".rubro option:selected").val();
    });

});