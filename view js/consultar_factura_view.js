$(document).on('ready', function () {
	var str_url = window.location.href;
	var t_proveedor;
	seleccionar_proveedor();

    $("#div_mostrar").css("display", "none");

	function seleccionar_proveedor() {
		var str_url_bc = str_url + '/seleccionar_proveedor/';
		$.post(str_url_bc, function (resultado) {
			$.each(resultado.registros, function (i, item) {
				$('#proveedor').append('<option value='+item.rif+' >' + item.nombre + '</option>');
			});
		}, 'json')

		.fail(function () {
			alert("error proveedor");
		});
	}

    $('#proveedor').on('change', function () {
        t_proveedor = $("#proveedor option:selected").val();
        tabla_listar_factura();
        tabla_factura.dataTable().fnClearTable();
        tabla_factura.dataTable().fnDestroy();
    });

	function tabla_listar_factura() {
        tabla_factura = $("#tabla_factura");
        var str_url_bc = str_url + '/tabla_listar_factura';
        $("#tabla_factura").DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "Ningun elemento encontrado",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrado de _MAX_ registros totales)",
                "search": "Búsqueda:",
                "info": "Mostrando _START_ - _END_ de _TOTAL_ registros",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "dom": 'lBfrtip',
                "paginate": {
                    "first": "Primera",
                    "last": "Última",
                    "next": "Sig",
                    "previous": "Ant"
                }
            },

            "ajax": {
                "url": str_url_bc,
                "dataSrc": "registros",
                "dataType": "json",
                "type": "POST",
                "data": {"t_proveedor": t_proveedor}
            },

            "columns": [
                {"data": "id_factura", "orderable": true},
                {"data": "rif"},
                {"data": "num_factura"},
                {"data": "fecha_recibido"},
                {
              
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": "<button class='glyphicon glyphicon-floppy-save btn btn-primary'></button>"

                }
            ],

            "initComplete": function (settings, resultado) {
                if (resultado.registros == '') {
                    $("#div_mostrar").css("display", "none");
                    alert('No existe dotacion registrada!!');
                } else {
                    $("#div_mostrar").show();
                    $("td").click(function() {
                        var oID = $(this).attr("id");
                        var prueba= oID.split("_");
                        if(prueba[0]=="factura"){
                            generar_reporte(prueba[1]);
                        }
                    });
                }
            },

            "createdRow": function (row, data, index) {
                $('td', row).eq(4).attr('id','factura_'+data.id_factura);
                $('td', row).eq(4).attr('style', 'cursor: pointer');
                $('td', row).eq(4).attr('title', 'Imprimir Reporte.');
            },
        });
    }
    
    function generar_reporte(id_factura) {
        var str_url_bc = str_url + '/imprimir_reporte/';
        var respuesta_ajax = $.ajax({
            url: str_url_bc,
            data: "id_factura=" + id_factura,
            cache: false,
            processData: false,
            type: 'POST',
            method: "post",
            error: function () {
                var msj = crear_mensaje("Error", "danger");
                ver_mensaje(msj);
            }
        }).always(function () {

        });
        $.when(respuesta_ajax).done(function (ajaxValue) {
            var win = window.open('', '_blank');
            win.location.href = ajaxValue;
            var msj = crear_mensaje("Proceso Finalizado", "info");
            ver_mensaje(msj);
        });

    }

});