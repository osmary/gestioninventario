$.busqueda = new Object();
$.id = 0;

$(document).on('ready', function() {
    var str_url = window.location.href;
    tabla_mostrar_proveedores();
	
    $('#guardar').on('click', function(){
		guardar_dotacion_proveedor();
        limpiar_campos();
	});

    $('#actualizar_proveedor').on('click', function(){
        actualizar_proveedor_existente();
    });

    $('#eliminar_proveedor').on('click', function(){
        eliminar_proveedor();
    });


	function tabla_mostrar_proveedores() {
        tabla_proveedor = $("#tabla_de_proveedores");
        var str_url_bc = str_url + '/tabla_proveedores/';
        $("#tabla_de_proveedores").DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "Ningun elemento encontrado",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtrado de _MAX_ registros totales)",
                "search": "Búsqueda:",
                "info": "Mostrando _START_ - _END_ de _TOTAL_ registros",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "paginate": {
                    "first": "Primera",
                    "last": "Última",
                    "next": "Sig",
                    "previous": "Ant"
                }
            },

            "ajax": {
                "url": str_url_bc,
                "dataSrc": "registros",
                "dataType": "json",
                "type": "POST",
            },

            "columns": [
                {"data": "rif", "orderable": true},
                {"data": "nombre"},
                {"data": "telefono"},
                {"data": "direccion"},
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data":      null,
                    "defaultContent": "<button class='glyphicon glyphicon-eye-open btn btn-primary'></button>"
                }
            ],


            "initComplete": function (settings, resultado) {
                if (resultado.registros == '') {
                    $("#div_proveedores").css("display", "none");
                    alert('No existe dotacion registrada!!');
                } else {
                    $("#div_proveedores").show();
                    $.busqueda = resultado;
                    $("td").click(function () {
                        var oID = $(this).attr("rif");
                        console.log(oID);
                        $.traer_registro(oID, $.busqueda.registros);
                        var prueba = oID.split("");
                        $("#myModal").modal();
                    });
                }

            },

            "createdRow": function (row, data, index) {
                if ($.id !== data.rif) {
                    $.id = data.rif;
                    $('td', row).eq(4).attr('rif', data.rif);
                    $('td', row).eq(4).attr('style', 'cursor: pointer');
                    $('td', row).eq(4).attr('title', 'Ver detalle');
                } else {
                    $('td', row).remove();
                }
            },
        });
    }

    $.traer_registro = function (id, items) {
        var oID = id;
        $.each(items, function (indice, reg) {
            if (oID === reg.rif) {
                oID = 0;
                $("#title_por_entregar").text(reg.rif +' - '+ reg.nombre);
                $.rif = reg.rif;
                seleccionar_ordenes(reg.rif);
            }
        });
    };


    function eliminar_proveedor(rif){
        var str_url_bc = str_url + '/eliminar_proveedor/';
        $.post(
            str_url_bc,
            {
                'rif': $('#modal-rif').val()
            },function(resultado) {
                if (resultado.mensaje == 'se elimino') {
                    alert('El proveedor se ha aliminado con exito');
                    tabla_proveedor.dataTable().fnClearTable();
                    tabla_proveedor.dataTable().fnDestroy();
                    tabla_mostrar_proveedores();
                    $("#myModal").modal('hide');

                } 
                else{
                    alert('Error al eliminar gerente');
                }
            },
            'json'
            )
            .fail(function() {
                alert("error 500");
            });
    }

    function aviso(){
        if (!confirm("Esta seguro que desea eliminar a este Proveedor?")) {
            return false;
        }
        else {
            return true;
        }
    } 



    function seleccionar_ordenes(id_rif) {
        var str_url_bc = str_url + '/seleccionar_ordenes/';
        limpiar_campos();
        $('#modal-rif').val(id_rif);
        $.post(str_url_bc, function (resultado) {
            $.each(resultado.registros, function (i, item) {
                if (id_rif === item.rif) {
                    $("#modal-rif").val(item.rif);
                    $("#modal-proveedor").val(item.nombre);
                    $("#modal-telefono").val(item.telefono);
                    $("#modal-direccion").val(item.direccion);
                }
            });
        }, 'json')
                .fail(function () {
                    alert("error al seleccionar orden");
                });
    }
    function limpiar_campos() {
        $("#rif").val("");
        $("#proveedor").val("");
        $("#telefono").val("");
        $("#direccion").val("");
    }
    

	function guardar_dotacion_proveedor() {
        var str_url_bc = str_url + '/guardar_proveedor/';
        $.post(str_url_bc, {
            'rif': $('#rif').val(),
            'nombre': $('#proveedor').val(),
            'direccion': $('#direccion').val(),
            'telefono': $('#telefono').val()
        }, 
        function (resultado) {
            if (resultado.mensaje == 'agregado') {
                alert('Relacion agregada con exito');
                tabla_proveedor.dataTable().fnClearTable();
                tabla_proveedor.dataTable().fnDestroy();
                tabla_mostrar_proveedores();
            }
            if (resultado.mensaje == 'existe') {
                alert('esta relacion ya existe');
            }
        }, 'json')
            .fail(function () {
                    alert("error al guardar");
            });
    }

    function actualizar_proveedor_existente() {
        var str_url_bc = str_url + '/actualizar_proveedor/';
        $.post(str_url_bc, {
            'rif': $('#modal-rif').val(),
            'nombre': $('#modal-proveedor').val(),
            'direccion': $('#modal-direccion').val(),
            'telefono': $('#modal-telefono').val()
        }, 
        function (resultado) {
            if (resultado.mensaje == 'actualizado') {
                tabla_proveedor.dataTable().fnClearTable();
                tabla_proveedor.dataTable().fnDestroy();
                tabla_mostrar_proveedores();
                $("#myModal").modal('hide');
            }
        }, 'json')
            .fail(function () {
                    alert("error al actualizar");
            });
    }

});